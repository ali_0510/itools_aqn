
    
  $(".trigger-pswd").click(function(){
              swal({   
                  title: "Change Password!",  
                  text: "Ketikan Password Baru Anda",  
                  type: "input",   showCancelButton: true,
                  inputType: "password",   
                  closeOnConfirm: false,   
                  inputPlaceholder: "Write Your New Password" 
              }, function(inputValue){   
                  if (inputValue === false) return false;      
                      
                  if (inputValue === "") {     
                      swal.showInputError("You need to write Password!");     
                      return false   
                  }      
                      
                  swal("Congrats!", "Your password has been changed into: " + inputValue, "success"); 
              });
          
          });

  $(window).resize(function(){
      $(".sweet-alert").css("margin-top",-$(".sweet-alert").outerHeight()/2);
  });

  $(".idleCa").click(function(){
      swal({
        closeOnConfirm: false,
        title: "Anda Belum Melakukan Settlement pada",
        text: "<table class='bordered settlement-tbl' style='font-size:12px;'><thead><tr><td>No.</td><td>CA Date</td><td>CA Code</td><td>Amount</td><td>Action</td></tr></thead><tbody><tr><td>1</td><td>25-11-2016</td><td>CA0002</td><td>Rp 10.000.000</td><td><a href='settlement' class='btn green'>settle</a></td></tr><tr><td>2</td><td>22-11-2016</td><td>CA0002</td><td>Rp 10.000.000</td><td><a href='settlement' class='btn green'>settle</a></td></tr><tr><td>3</td><td>19-11-2016</td><td>CA0002</td><td>Rp 10.000.000</td><td><a href='' class='btn green'>settle</a></td></tr><tr><td>4</td><td>10-11-2016</td><td>CA0002</td><td>Rp 10.000.000</td><td><a href='' class='btn green'>settle</a></td></tr><tr><td>5</td><td>05-11-2016</td><td>CA0002</td><td>Rp 10.000.000</td><td><a href='' class='btn green'>settle</a></td></tr></tbody></table>",
        html: true
      });
      
  });
    

  $(".dropthis").click(function(){
  		$(".content-drop").slideToggle();
  });

  document.getElementById('annual').addEventListener('change', function () {
    var style = this.value == 3 ? 'block' : 'none';
    document.getElementById('special-leave').style.display = style;
  });


