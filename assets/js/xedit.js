$(document).ready(function() {
              //toggle `popup` / `inline` mode
              $.fn.editable.defaults.mode = 'inline';              
              //defaults
              // $.fn.editable.defaults.url = '/post'; 
              //make username editable
              $('[data-edit="editable"]').editable();
              $('.edit-post-button').on('click', function(e) {
               
                    $('#employee .editable').editable('toggle');        
              });
              $('.myeditable').editable();  
         
              $('.myeditable').editable();
                  $('[data-currency="amount"]').on('shown', function () {
                  $(this).data('editable').input.$input.maskMoney({
                    prefix:'Rp  ', 
                    allowNegative: true, 
                    thousands:'.', 
                    decimal:',', 
                    affixesStay: false
                  });
              });

              
                $('[data-gender="sex"]').editable({
                  prepend: "not selected",
                  source: [
                      {value: 1, text: 'Male'},
                      {value: 2, text: 'Female'}
                  ],
                  display: function(value, sourceData) {
                       var colors = {"": "gray", 1: "green", 2: "blue"},
                           elem = $.grep(sourceData, function(o){return o.value == value;});
                           
                       if(elem.length) {    
                           $(this).text(elem[0].text).css("color", colors[value]); 
                       } else {
                           $(this).empty(); 
                       }
                  }   
              }); 

              $("[data-type='textarea']").on("shown", function (element, test) {
                  $(test.input.$input[0]).attr("rows", "3")            
              });

              $('#datetaken').editable({
                  placement: 'right',
                  combodate: {
                      firstItem: 'name',
                      maxYear: '2030'
                  }
              });         
              
              var reimburse = [];
              $.each({"PLS": "Pulse", "TRP": "Transport"}, function(k, v) {
                  reimburse.push({id: k, text: v});
              }); 
              $('#typeReimburse').editable({
                  source: reimburse,
                  select2: {
                      width: 200,
                      placeholder: 'Select Type Reimbursement',
                      allowClear: true
                  } 
              });  


              var office = [];
              $.each({"PLS": "Pulse", "MDC": "Medical"}, function(k, v) {
                  office.push({id: k, text: v});
              }); 
              $('#typeOffice').editable({
                  source: office,
                  select2: {
                      width: 200,
                      placeholder: 'Select Type Reimbursement',
                      allowClear: true
                  } 
              });
              // var reimbursenon = [];
              // $.each({"PLS": "Pulse", "MDC": "Medical"}, function(k, v) {
              //     reimbursenon.push({id: k, text: v});
              // }); 
              // $('#typeReimbursenon').editable({
              //     source: reimbursenon,
              //     select2: {
              //         width: 200,
              //         placeholder: 'Select Type Reimbursement',
              //         allowClear: true
              //     } 
              // });  

              var cacode = [];
              $.each({"CD001": "CD001", "CD002": "CD002"}, function(k, v) {
                  cacode.push({id: k, text: v});
              }); 
              $('#caCode').editable({
                  source: cacode,
                  select2: {
                      width: 200,
                      placeholder: 'Select Relevant Code',
                      allowClear: true
                  } 
              }); 

              
});

function returnAccess()
{

$.fn.editable.defaults.mode = 'inline';
$('.myeditable').editable(); 
$('.myeditable').editable();
      $('[data-currency="amount"]').on('shown', function () {
      $(this).data('editable').input.$input.maskMoney({
        prefix:'Rp  ', 
        allowNegative: true, 
        thousands:'.', 
        decimal:',', 
        affixesStay: false
      });
  });
$('[data-id="datetaken"]').editable({
    placement: 'right',
    combodate: {
        firstItem: 'name',
        maxYear: '2030'
    }
});

var reimburseproj = [];
$.each({"PLS": "Pulse", "TRP": "Transport"}, function(k, v) {
    reimburseproj.push({id: k, text: v});
}); 
$('[data-id="typeReimburseproj"]').editable({
    source: reimburseproj,
    select2: {
        width: 200,
        placeholder: 'Select Type Reimbursement',
        allowClear: true
    } 
});     
//ajax emulation
$.mockjax({
    url: '/post',
    responseTime: 200
}); 
var count = 2;
$("[data-id='addrow-reimburse']").click(function(){
           
    randomID = Math.floor(Math.random()*1000001);
    var col = "<tr><td id='number'>"+ (count++) +"</td><td><a href='javascript:;' data-id='datetaken' data-type='combodate' data-format='YYYY-MM-DD' data-viewformat='DD/MM/YYYY' data-template='D / MMM / YYYY' data-pk='"+randomID+"'  data-title='Select Date of Bill'>null</a></td><td><a href='#' data-pk='"+randomID+"'  data-id='typeReimburse' data-type='select2' data-title='Select Type'>null</a></td><td><a href='#' data-pk='"+randomID+"' class='myeditable'>null</a></td><td><a href='#' data-pk='"+randomID+"' class='myeditable'>null</a></td><td><button type='button' data-id='addrow-reimburse' class='waves-effect waves-light btn green' style='margin-right:10px;'>Save</button><button type='button' id='delete-btn' class='waves-effect waves-light btn red'>Delete</button></td></tr>" 
    $( "#editableRow" ).append(col);    
    returnAccess(); 
    
});

var count = 2;
$("[data-id='addrow']").click(function(){
           
    randomID = Math.floor(Math.random()*1000001);
    var col = "<tr><td id='number'>"+ (count++) +"</td><td><a href='javascript:;' data-id='datetaken' data-type='combodate' data-format='YYYY-MM-DD' data-viewformat='DD/MM/YYYY' data-template='D / MMM / YYYY' data-pk='"+randomID+"'  data-title='Select Date of Bill'>null</a></td><td><a href='#' data-pk='"+randomID+"'  data-id='typeReimburse' data-type='select2' data-title='Select Type'>null</a></td><td><a href='#' data-pk='"+randomID+"' class='myeditable'>null</a></td><td><a href='#' data-pk='"+randomID+"' class='myeditable'>null</a></td><td><a href='#' data-pk='"+randomID+"' class='myeditable'>null</a></td><td style='text-align:right'><button type='button' data-id='addrow-reimburse' class='waves-effect waves-light btn green' style='margin-right:10px;'>Save</button><button type='button' id='delete-btn' class='waves-effect waves-light btn red'>Delete</button></td></tr>" 
    $( "#editableRow" ).append(col);    
    returnAccess(); 
    
});

}

// trigger function in the begining 
returnAccess();

