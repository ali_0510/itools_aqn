$(document).ready(function() {
      //toggle `popup` / `inline` mode
      $.fn.editable.defaults.mode = 'inline';              
      //defaults
      // $.fn.editable.defaults.url = '/post'; 
      //make username editable
      $('[data-edit="editable"]').editable();
      $('.edit-post-button').on('click', function(e) {
       
            $('#employee .editable').editable('toggle');        
      });
      $('.editable').editable(); 

      
        $('[data-gender="sex"]').editable({
          prepend: "not selected",
          source: [
              {value: 1, text: 'Male'},
              {value: 2, text: 'Female'}
          ],
          display: function(value, sourceData) {
               var colors = {"": "gray", 1: "green", 2: "blue"},
                   elem = $.grep(sourceData, function(o){return o.value == value;});
                   
               if(elem.length) {    
                   $(this).text(elem[0].text).css("color", colors[value]); 
               } else {
                   $(this).empty(); 
               }
          }   
      }); 

      $("[data-type='textarea']").on("shown", function (element, test) {
          $(test.input.$input[0]).attr("rows", "3")            
      });
 
});

