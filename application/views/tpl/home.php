<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->load->view('tpl/header'); ?>
<main class="mn-inner">
    <div class="row">
        <div class="col s12">
            <div class="well">
                <h4>Selamat datang!</h4>
                <hr/>
                <p>Hai <span class="black-text"><?php echo $nama_lengkap?></span>. Selamat datang di <em>'Sistem Internal PT Asia Quattro Net' .</em> Anda dapat menggunakan fitur sistem melalui menu di panel bagian atas.  </p>
            </div>
        </div>
    </div>
</main>
<?php $this->load->view('tpl/footer'); ?>