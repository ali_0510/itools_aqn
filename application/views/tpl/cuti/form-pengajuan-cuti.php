<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->load->view('tpl/header'); ?>
<main class="mn-inner" style="padding:0 80px">
    <div class="row">
        <div class="col s12">
            <h5>Pengajuan Cuti</h5>
            <hr/>
           
            <form class="form-custom" action="<?php echo site_url();?>/cuti/create_cuti">
                <div class="col s6">
                    <div class="row">
                        <div class="input-field  col s6 no-padding">
                            <input id="applicant" type="text" class="validate" disabled value="Desmon">
                            <label for="applicant" class="active">Nama</label>
                        </div>                               
                    </div>
                    <div class="row">
                        <div class="input-field no-padding">
                            
                            <select class="browser-default">
                                <option value="" disabled selected>Choose your option</option>
                                <option value="1">Annual Leave</option>
                                <option value="2">Unpaid Leave</option>
                                <option value="3">Special Leave</option>
                            </select>
                            <label class="active" style="top:0">Particulars</label>
                            
                        </div>                  
                    </div>
                    
                    <div class="row">
                        <div class="input-field col s5 no-padding" style="margin-right:84px;">
                            <input id="annual-start" type="text" class="datepicker">
                            <label for="annual-start" class="active">Tanggal Mulai cuti</label>
                        </div>
                        <div class="input-field col s5 no-padding">
                            <input id="annual-end" type="text" class="datepicker">
                            <label for="annual-end" class="active">Sampai Tanggal</label>
                        </div>
                    </div>

                    <div class="row">
                        <div class="input-field col s5 no-padding" style="margin-right:84px;">
                            <input id="totalofdays" type="text" class="validate" >
                            <label for="totalofdays" class="active">Jumlah Cuti</label>
                        </div>
                        <div class="input-field col s5 no-padding">
                            <input id="substitution" type="text" class="validate" >
                            <label for="substitution" class="active">Pengganti Saat Cuti</label>
                        </div>                               
                    </div>

                    <div class="row">
                        <div class="col s12 no-padding" style="margin-right:5%">
                            <label class="active" style="top:0">Supervisor</label>                       
                            <select class="browser-default" style="margin-top:12px;">
                                <option>Saut M</option>
                                <option>Priyadi Budi</option>
                                <option>Ade Kurnia</option>
                            </select>
                        </div>
                    </div>

                    <div class="row">
                        <div class="input-field  no-padding">
                            <textarea id="textarea1" class="materialize-textarea" length="160"></textarea>
                            <label for="textarea1" class="active">Keterangan</label>
                        </div>
                    </div>
                     <span><input type="button" name="reset" value="Reset" class="waves-effect waves-light btn red"></span>
                     <span><input type="submit" name="submit" value="Ajukan" class="waves-effect waves-light btn green 
                     sweetalert-success" ></span>
                </div>

                
                <div class="col s4 right">
                    <ul class="collection">
                      <li class="collection-item"><span class="cl_name">Remaining Balance</span><span class="cl_value">8 Days</span></li>
                      <li class="collection-item"><span class="cl_name">Leave Entitlement</span><span class="cl_value">5 Days</span></li>
                      <li class="collection-item"><span class="cl_name">Leave Already Taken in</span><span class="cl_value">4 Days</span></li>
                      
                    </ul>
                </div>

            </form>
        </div>
    </div>
</main>
<div class="clearfix"></div>
<?php $this->load->view('tpl/footer'); ?>
