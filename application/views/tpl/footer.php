            <footer>
                <div class="center-align">Copyright &copy; 2016 | Asia Quattro</div>
            </footer>
        </div>
 
        
        <!-- Javascripts -->
        <script src="<?php echo base_url();?>assets/plugins/jquery/jquery-2.2.0.min.js"></script>        
        <script src="<?php echo base_url();?>assets/plugins/materialize/js/materialize.min.js"></script>
        <script src="<?php echo base_url();?>assets/plugins/material-preloader/js/materialPreloader.min.js"></script>        
        <script src="<?php echo base_url();?>assets/plugins/jquery-blockui/jquery.blockui.js"></script>
        <script>
           $(document).ready(function() {
              $("#navAjax li a").click(function() {
           
                  $("#ajax-content").empty().append("<div id='loading' class='center-align'><img src='<?php echo base_url();?>assets/images/Loading_icon.gif' alt='Loading' / width='100'></div>");
                  $("#navAjax li a").removeClass('current');
                  $(this).addClass('current');
           
                  $.ajax({ url: this.href, success: function(html) {
                      $("#ajax-content").empty().append(html);
                      }
              });
              return false;
              });
           
              $("#ajax-content").empty().append("<div id='loading' class='center-align'><img src='<?php echo base_url();?>assets/images/Loading_icon.gif' alt='Loading' / width='100'></div>");
              $.ajax({ url: '<?php echo site_url();?>/User/ajxAnnual/', success: function(html) {
                      $("#ajax-content").empty().append(html);
              }
              });

              
          });
        </script> 
        <script src="<?php echo base_url();?>assets/js/jquery.mockjax.js"></script>
        <script src="<?php echo base_url();?>assets/js/moment.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/bootstrap-datetimepicker.js"></script>
        <script src="<?php echo base_url();?>assets/js/select2.js"></script>
        <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/bootstrap-editable.js"></script>
        <script src="<?php echo base_url();?>assets/js/xedit.js"></script>
        <script src="<?php echo base_url();?>assets/js/jquery.maskMoney.js"></script>
        <script src="<?php echo base_url();?>assets/js/alpha.min.js"></script>
        <script src="<?php echo base_url();?>assets/plugins/datatables/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $('.datatable-example').DataTable({
                    language: {
                        searchPlaceholder: 'Search records',
                        sSearch: '',
                        sLengthMenu: 'Show _MENU_',
                        sLength: 'dataTables_length',
                        oPaginate: {
                            sFirst: '<i class="material-icons">chevron_left</i>',
                            sPrevious: '<i class="material-icons">chevron_left</i>',
                            sNext: '<i class="material-icons">chevron_right</i>',
                            sLast: '<i class="material-icons">chevron_right</i>' 
                    }
                    }
                });
                $('.dataTables_length select').addClass('browser-default');
            });
        </script>
        <script src="<?php echo base_url();?>assets/js/pages/form_elements.js"></script>
        <script src="<?php echo base_url();?>assets/plugins/sweetalert/sweetalert.min.js"></script>      
        <script src="<?php echo base_url();?>assets/js/pages/miscellaneous-sweetalert.js"></script>
        <script src="<?php echo base_url();?>assets/js/custom.js"></script>


    </body>
</html>