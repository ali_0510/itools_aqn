<?php $this->load->view('admin/header'); ?>

<main class="mn-inner" style="padding:0 80px">
    <div class="row">
        <div class="col s12">
            <h5>Settlement</h5>
            <hr/>
          
            <form class="form-custom" action="">
            	<div class="row">
            		<div class="col s1 no-padding">
            			<label>Date Submited</label>
            			<input type="text" name="" disabled value="03-11-2016">
            		</div>
            	</div>
                <div class="row">
                    <div class="col s3" style="padding-left:0; margin-right:5%">
                        <label>Name</label>
                        <input type="text" name="" disabled value="Desmon">
                    </div>
                    <div class="col s2">
                        <label>Settlement</label>
                        <input type="text" name="" disabled value="">
                    </div>
                </div>

                <table class="display datatable-example responsive-table highlight striped" id="editableRow">
                	 	<thead>
                            <tr>
                            	<th>No</th>
                                <th width="150">Date of Bill</th>
                                <th width="250">Description</th>
                                <th>Amount</th>
                                <th>Projects Code</th>
                                <th width="150">CA Code</th>
                                <th style="text-align:right">Action</th>
                            </tr>
                    	</thead>
	                    <tfoot>
	                        <tr>
    	                        <th>No</th>
                                <th width="150">Date of Bill</th>
                                <th width="250">Description</th>
                                <th>Amount</th>
                                <th>Projects Code</th>
                                <th width="150">CA Code</th>
                                <th style="text-align:right">Action</th>
	                        </tr>
	                    </tfoot>
	                    <tbody>
	                        <tr>
	                        	<td>1</td>
	                            <td><a href="javascript:;" id="datetaken" data-type="combodate" data-format="YYYY-MM-DD" data-viewformat="DD/MM/YYYY" data-template="D / MMM / YYYY" data-pk="1"  data-title="Select Date of Bill">null</a></td>
	                            <td><a href="javascript:;" data-type="textarea" data-pk="4" class="myeditable">null</a></td>
	                            <td><a href="javascript:;" data-currency="amount" data-pk="5" class="myeditable">null</a></td>
	                            <td><a href="javascript:;" id="projects_code" data-pk="6" class="myeditable">null</a></td>
                                <td><a href="javascript:;" id="caCode" data-type="select2" data-pk="1" data-value="" data-title="Select Relevant Code">null</a></td>
                                <td style="text-align:right"><button type="button" data-id="addrow-reimburse" id="save-btn" class="waves-effect waves-light btn green" style="margin-right:10px;">Save</button><button type="button" id="delete-btn" class="waves-effect waves-light btn red">Delete</button></td>
                            </tr>
	                        </tr>

	                        
	                   </tbody>
                </table>

                <span><input type="button" name="reset" value="Reset" class="waves-effect waves-light btn red"></span>
                <span><input type="submit" name="submit" value="Ajukan" class="waves-effect waves-light btn green sweetalert-success" ></span>
            </form>
        </div>
    </div>
</main>
<div class="clearfix"></div>

<?php $this->load->view('admin/footer'); ?>

