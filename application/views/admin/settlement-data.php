<?php $this->load->view('admin/header'); ?>   
<main class="mn-inner" style="padding:0 80px">
    <div class="row">
        <div class="col s12">
            <h5>Settlement Data Employee</h5>
            <hr/>
            <table id="example" class="display responsive-table datatable-example bordered">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>Date Submit</th>
                        <th>Date Of Bill</th>
                        <th>Projects</th>
                        <th width="250">Description</th>
                        <th>Status</th>
                        <th>Action</th>                        
                        
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>Date Submit</th>
                        <th>Date Of Bill</th>
                        <th>Projects</th>
                        <th width="250">Description</th>
                        <th>Status</th>
                        <th>Action</th>                        
                        
                    </tr>
                </tfoot>
                <tbody>
                    <tr>
                        <td>1</td>
                        <td>Desmon</td>
                        <td>11/11/2016</td>
                        <td>15/11/2016</td>
                        <td>Kineria</td>
                        <td>1 unit Lenovo E450</td>
                        <td>
                            <ul class="nav-icon-action">
                                <li><div class="badge-status"><span class="badge yellow">Pending</span></div></li>
                                
                            </ul>
                        </td>    
                        <td>
                            <ul class="nav-icon-action">
                                <li><a href="<?php echo site_url() ?>/admin/viewDetaildatasettle" title="view detail" class="btn white-text">Detail</a></li>
                                
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>Ali</td>
                        <td>08/11/2016</td>
                        <td>12/11/2016</td>
                        <td>Kineria</td>
                        <td>1 unit Lenovo E450</td>
                        <td>
                            <ul class="nav-icon-action">
                                <li><div class="badge-status"><span class="badge yellow">Pending</span></div></li>
                                
                            </ul>
                        </td>                                  
                        <td>
                            <ul class="nav-icon-action">
                                <li><a href="<?php echo site_url() ?>/admin/viewDetaildatasettle" title="view detail" class="btn white-text">Detail</a></li>
                                
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>Adit</td>
                        <td>03/11/2016</td>
                        <td>05/11/2016</td>
                        <td>Kineria</td>
                        <td>1 unit Lenovo E450</td>
                        <td>
                            <ul class="nav-icon-action">
                                <li><div class="badge-status"><span class="badge yellow">Pending</span></div></li>
                                
                            </ul>
                        </td>                                  
                        <td>
                            <ul class="nav-icon-action">
                                <li><a href="<?php echo site_url() ?>/admin/viewDetaildatasettle" title="view detail" class="btn white-text">Detail</a></li>
                                
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td>Dudi</td>
                        <td>07/11/2016</td>
                        <td>09/11/2016</td>
                        <td>Kineria</td>
                        <td>1 unit Lenovo E450</td>
                        <td>
                            <ul class="nav-icon-action">
                                <li><div class="badge-status"><span class="badge yellow">Pending</span></div></li>
                                
                            </ul>
                        </td>                                  
                        <td>
                            <ul class="nav-icon-action">
                                <li><a href="<?php echo site_url() ?>/admin/viewDetaildatasettle" title="view detail" class="btn white-text">Detail</a></li>
                                
                            </ul>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</main>
<div class="clearfix"></div>

           
<?php $this->load->view('admin/footer'); ?>   

        
   