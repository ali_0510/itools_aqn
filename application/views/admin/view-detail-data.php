<?php $this->load->view('admin/header'); ?>
<main class="mn-inner">
            <div class="row">

                <div class="col s12 m4 l3">
                    <div class="card">
                        <div class="card-content center-align">
                            <img src="<?php echo base_url() ?>assets/images/profile-image-2.png" class="responsive-img circle" width="128px" alt="">
                            <p class="m-t-lg flow-text">Desmon William</p>
                            <div class="chip m-t-sm blue-grey white-text">UX/UI Designer</div>
                            
                        </div>

                        <div class="card-content">
                   
                           <ul class="collection" style="border:none;float:none">
                              <li class="collection-item"><span>Leave Balance 2015 :</span>
                                <span class="total_leave">5</span></li>
                              <li class="collection-item"><span>Leave Balance 2016 :</span>
                                <span class="total_leave">6</span></li>
                            </ul>

                        </div>

                        
                    </div>



                </div>
                <div class="col s12 m4 l9">

                    <div class="card">
                        <div class="card-content ">
                        
                         <div class="right-align">Status : <div class=" chip m-t-sm blue white-text" data-id="badge">New</div> <span id="print" style="display:none"><a href="printout-cuti.html" class="waves-effect waves-grey btn-flat "><i class="material-icons">print</i></a></span></div>
                            <ul class="collection" style="border:none">
                                  
                                  <li class="collection-item"><span class="cl_name">Particulars</span><span class="cl_value">Annual Leave</span></li>
                                  <li class="collection-item"><span class="cl_name">Jumlah Cuti yang diambil</span><span class="cl_value">5 Hari</span></li>                                  
                                  <li class="collection-item"><span class="cl_name">Sisa Cuti</span><span class="cl_value">7 Hari</span></li>
                                  <li class="collection-item"><span class="cl_name">Nama Pengganti Cuti</span><span class="cl_value">Ahmad Suhaili</span></li>
                                  <li class="collection-item"><span class="cl_name">Tgl Pengajuan</span><span class="cl_value">11/12/2016</span></li>
                                  <li class="collection-item"><span class="cl_name">Supervisor</span><span class="cl_value">Intan Gustiarti</span></li>
                                  <li class="collection-item"><span class="cl_name">Periode Cuti</span><span class="cl_value">11/12/2016 <span>sampai</span> 16/12/2016</span></li>
                                   <li class="collection-item"><span class="cl_name">Keterangan Cuti</span><span class="cl_value">Pulang Kampung</span></li>
                                  
                                  
                            </ul>
                        </div>
                        <div class="card-content ">
                            <span class="card-title">Info Pengganti</span>
                            <p>Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus.</p>

                            <ul class="nav-icon-action center-align">
                                <li id="approval"><a href="javascript:;" data-id="approve" title="approve" class="waves-effect waves-grey btn green white-text ">Terima</a></li>
                                <li id="rejected"><a href="javascript:;" title="tolak" class="waves-effect waves-grey btn-flat ">
                                    Tolak</a></li>
                                
                                
                            </ul>
                        </div>
                        
                    </div>
                </div>

            </div>
</main>


<div class="clearfix"></div>
<?php $this->load->view('admin/footer'); ?>   