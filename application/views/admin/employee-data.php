<?php $this->load->view('admin/header'); ?>  
            <main class="mn-inner" style="padding: 0 80px;">
                <div class="row">
                    <div class="col s12">
                        <h5>Employee Data</h5>
                        <hr/>
                        <a class="waves-effect waves-light btn green mb-20 " href="<?php echo site_url();?>/Admin/addUser">Tambah Pegawai</a>
                       
                        <table id="employee" class="display responsive-table datatable-example bordered media-body">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th width="150">NIP</th>
                                    <th width="150">Nama Lengkap</th>
                                    <th >Jenis Kelamin</th>
                                    <th width="250">Alamat</th>
                                    <th>No. Telepon</th>                                    
                                    <th>Status</th>
                                    <th style="text-align:center">Action</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>No</th>
                                    <th width="150">NIP</th>
                                    <th width="150">Nama Lengkap</th>
                                    <th >Jenis Kelamin</th>
                                    <th width="250">Alamat</th>
                                    <th>No. Telepon</th>                                    
                                    <th>Status</th>
                                    <th style="text-align:center">Action</th>
                                </tr>
                            </tfoot>
                            <tbody>
                                <tr>
                                    <td><a class="editable" data-title="Edit your post" data-pk="1" data-type="text">1</a></td>
                                    <td><a class="editable" data-title="Edit your post" data-pk="2" data-type="text">PG0001</a></td>
                                    <td><a class="editable" data-title="Edit your post" data-pk="3" data-type="text">Andy Gumilang</a></td>
                                    <td><a class="editable" id="gender" data-title="Edit your post" data-pk="4" data-gender="sex">Laki-Laki</a></td>
                                    <td><a class="editable" data-title="Edit your post" data-pk="5" data-type="textarea">Jakarta kota seribu kenangan</a></td>
                                    <td><a class="editable" data-title="Edit your post" data-pk="6" data-type="text">08772662627</a></td>                                    
                                    <td><div class="badge-status"><span class="badge green">Active</span></div></td>
                                    <td>
                                        <ul class="nav-icon-action-emp">
                                            <li><a href="<?php echo site_url() ?>/Admin/viewDetail" title="view detail" class="waves-effect waves-grey btn-flat"><i class="material-icons">visibility</i></a></li>                                            
                                            <li><a class="edit-post-button waves-effect waves-grey btn-flat" href="javascript:;">Edit</a></li>
                                            <li><form><input type="submit" name="" value="save" class="waves-effect waves-grey btn-flat"></form></li>
                                            
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td><a class="editable" data-title="Edit your post" data-pk="1" data-type="text">2</a></td>
                                    <td><a class="editable" data-title="Edit your post" data-pk="1" data-type="text">PG0002</a></td>
                                    <td><a class="editable" data-title="Edit your post" data-pk="1" data-type="text">Ari Lasso</a></td>
                                    <td><a class="editable" data-title="Edit your post" data-pk="1" data-type="text">Laki-Laki</a></td>
                                    <td><a class="editable" data-title="Edit your post" data-pk="1" data-type="text">Bandung Lautan Cinta</a></td>
                                    <td><a class="editable" data-title="Edit your post" data-pk="1" data-type="text">087726626272</a></td>                                   
                                    <td><div class="badge-status"><span class="badge green">Active</span></div></td>
                                    <td>
                                        <ul class="nav-icon-action-emp">
                                            <li><a href="<?php echo site_url() ?>/Admin/viewDetail" title="view detail" class="waves-effect waves-grey btn-flat"><i class="material-icons">visibility</i></a></li>                                            
                                            <li><a class="edit-post-button waves-effect waves-grey btn-flat" href="javascript:;">Edit</a></li>
                                            <li><form><input type="submit" name="" value="save" class="waves-effect waves-grey btn-flat"></form></li>
                                            
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td><a class="editable" data-title="Edit your post" data-pk="1" data-type="text">3</a></td>
                                    <td><a class="editable" data-title="Edit your post" data-pk="1" data-type="text">PG0003</a></td>
                                    <td><a class="editable" data-title="Edit your post" data-pk="1" data-type="text">Iwan Males</a></td>
                                    <td><a class="editable" data-title="Edit your post" data-pk="1" data-type="text">Laki-Laki</a></td>
                                    <td><a class="editable" data-title="Edit your post" data-pk="1" data-type="text">Sumedang yang banyak tahu</a></td>
                                    <td><a class="editable" data-title="Edit your post" data-pk="1" data-type="text">087732662627</a></td>                                    
                                    <td><div class="badge-status"><span class="badge green">Active</span></div></td>
                                    <td>
                                        <ul class="nav-icon-action-emp">
                                            <li><a href="<?php echo site_url() ?>/Admin/viewDetail" title="view detail" class="waves-effect waves-grey btn-flat"><i class="material-icons">visibility</i></a></li>                                            
                                            <li><a class="edit-post-button waves-effect waves-grey btn-flat" href="javascript:;">Edit</a></li>
                                            <li><form><input type="submit" name="" value="save" class="waves-effect waves-grey btn-flat"></form></li>
                                            
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td><a class="editable" data-title="Edit your post" data-pk="1" data-type="text">4</a></td>
                                    <td><a class="editable" data-title="Edit your post" data-pk="1" data-type="text">PG0004</a></td>
                                    <td><a class="editable" data-title="Edit your post" data-pk="1" data-type="text">Arman Dole</a></td>
                                    <td><a class="editable" data-title="Edit your post" data-pk="1" data-type="text">Laki-Laki</a></td>
                                    <td><a class="editable" data-title="Edit your post" data-pk="1" data-type="text">Papua</a></td>
                                    <td><a class="editable" data-title="Edit your post" data-pk="1" data-type="text">087742662627</a></td>                                   
                                    <td><div class="badge-status"><span class="badge green">Active</span></div></td>
                                    <td>
                                        <ul class="nav-icon-action-emp">
                                            <li><a href="<?php echo site_url() ?>/Admin/viewDetail" title="view detail" class="waves-effect waves-grey btn-flat"><i class="material-icons">visibility</i></a></li>                                            
                                            <li><a class="edit-post-button waves-effect waves-grey btn-flat" href="javascript:;">Edit</a></li>
                                            <li><form><input type="submit" name="" value="save" class="waves-effect waves-grey btn-flat"></form></li>
                                            
                                        </ul>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </main>
            <div class="clearfix"></div>


<?php $this->load->view('admin/footer'); ?>  