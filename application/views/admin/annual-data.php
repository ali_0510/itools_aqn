<?php $this->load->view('admin/header'); ?>   
<main class="mn-inner" style="padding:0 80px">
    <div class="row">
        <div class="col s12">
            <h5>Annual Data Employee</h5>
            <hr/>
            <a href="javascript:;" class="btn trigger-status">Filter Status </a>
            <div id="divStatus" class="filterS" style="display:none">
                <a href="javascript:;" class="closeTrigger">X</a>
                <form>
                    <select>
                         <option>New</option>
                         <option>Diterima</option>
                         <option>Ditolak</option>
                     </select>
                     <input type="submit" name="sFilter" class="btn" style="margin:-8px 0 10px;">
                </form>   
            </div>
            <table id="example" class="display responsive-table datatable-example bordered">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>Particulars</th>
                        <th>Tgl Pengajuan</th>
                        <th>Keterangan Cuti</th>
                                                
                        <th>Status</th>
                        <th width="150">Action</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>Particulars</th>
                        <th>Tgl Pengajuan</th>
                        <th>Keterangan Cuti</th>
                                                  
                        <th>Status</th>
                        <th width="150">Action</th>
                    </tr>
                </tfoot>
                <tbody>
                    <tr>
                        <td>1</td>
                        <td>Aji Notomo</td>
                        <td>Annual Leave</td>
                        <td>11/11/2016</td>
                        <td>Ijin Pulang Kampung </td>
                                                        
                        <td>
                            <ul class="nav-icon-action">
                                <li><div class="badge-status"><span class="badge blue" data-id="badge">New</span></div></li>
                            </ul>
                        </td>
                        <td>
                            <ul class="nav-icon-action">
                                <li><a href="<?php echo site_url() ?>/admin/viewDetaildata" title="view detail" class="btn white-text">Detail</a></li>
                                
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>Desmon</td>
                        <td>Annual Leave</td>
                        <td>15/11/2016</td>
                        <td>Ijin Pulang Kampung </td>
                                                       
                        <td>
                            <ul class="nav-icon-action">
                                <li><div class="badge-status"><span class="badge green">Diterima</span></div></li>
                                
                            </ul>
                        </td>
                        <td>
                            <ul class="nav-icon-action">
                                <li><a href="<?php echo site_url() ?>/admin/viewDetaildata" title="view detail" class="btn white-text">Detail</a></li>
                                
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>Ali</td>
                        <td>Annual Leave</td>
                        <td>08/11/2016</td>
                        <td>Ijin Pulang Kampung </td>
                                                        
                        <td>
                            <ul class="nav-icon-action">
                                <li><div class="badge-status"><span class="badge blue" data-id="badge2">New</span></div></li>
                                
                            </ul>
                        </td>
                        <td>
                            <ul class="nav-icon-action">
                                <li><a href="<?php echo site_url() ?>/admin/viewDetaildata" title="view detail" class="btn white-text">Detail</a></li>
                               
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td>Dudi</td>
                        <td>Annual Leave</td>
                        <td>05/11/2016</td>
                        <td>Ijin Pulang Kampung </td>
                                                       
                        <td>
                            <ul class="nav-icon-action">
                                <li><div class="badge-status"><span class="badge yellow">Menunggu</span></div></li>
                                
                            </ul>
                        </td>
                        <td>
                            <ul class="nav-icon-action">
                                <li><a href="<?php echo site_url() ?>/admin/viewDetaildata" title="view detail" class="btn white-text">Detail</a></li>
                                
                            </ul>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</main>
<div class="clearfix"></div>

           
<?php $this->load->view('admin/footer'); ?>   

        
   