
<?php $this->load->view('admin/header'); ?>
<main class="mn-inner">
    <div class="row">
        <div class="col s12">
            <h5>Add User</h5>
            <hr/>
            <a class="waves-effect waves-light btn red mb-20" href="<?php echo site_url();?>/Admin/employeeData">Kembali</a>
            <form class="col s12 form-custom">
                <div class="row">
                    <div class="input-field col s5 left no-padding">
                        <input type="text" name="username" class="validate" id="username" placeholder=" ">
                        <label for="username" class="active">Username</label>
                    </div>
                    <div class="input-field col s5 right no-padding">
                        <input type="text" name="nip" class="validate" id="nip" placeholder=" ">
                        <label for="nip" class="active">NIP</label>
                    </div>                            
                </div>
               <div class="row">
                    <div class="input-field col s5 left no-padding">
                        <input type="password" name="password" class="validate" id="password" placeholder="">
                        <label for="password" class="active">Password</label>
                    </div>
                    <div class="input-field col s5 right no-padding">
                        <input type="text" name="ktp" class="validate" id="ktp" placeholder=" ">
                        <label for="ktp" class="active">No. KTP</label>
                    </div>  
                </div>
                <div class="row">
                    <div class="input-field col s5 no-padding left">
                        <input type="text" name="nameuser" class="validate" id="nameuser" placeholder=" ">
                        <label for="nameuser" class="active">Nama Lengkap</label>
                    </div>
                    <div class="input-field col s5 no-padding right">
                        <input type="text" name="position" class="validate" id="position" placeholder=" ">
                        <label for="position" class="active">Jabatan</label>
                    </div>
                                            
                </div>
                <div class="row">
                    <div class="input-field col s5 no-padding left">
                        <input type="text" name="notelpon" class="validate" id="notelpon" placeholder=" ">
                        <label for="notelpon" class="active">No. Telephone</label>
                    </div>
                    
                    <div class="input-field col s5 no-padding right">
                        <input type="email" name="email" class="validate" id="email" placeholder=" ">
                        <label for="email" class="active">Email</label>
                    </div>                           
                </div>
                <div class="row">
                    <div class="input-field col s12 no-padding">
                        <textarea id="textarea1" class="materialize-textarea" length="250" placeholder=" "></textarea>
                        <label for="textarea1" class="active">Alamat</label>
                    </div>
                </div>
                
                <div class="row">
                    <div class="input-field col s4 left">
                        <select>
                            <option value="" disabled selected>Pilih Jenis Kelamin</option>
                            <option value="1">Laki-Laki</option>
                            <option value="2">Perempuan</option>
                            <option value="3">Others</option>
                        </select>
                        <label>Jenis Kelamin</label>
                    </div>
                    <div class="input-field col s4  left">
                        <select>
                            <option value="" disabled selected>Pilih Agama</option>
                            <option value="1">Islam</option>
                            <option value="2">Kristen</option>
                            <option value="3">Katholik</option>
                            <option value="4">Hindu</option>
                            <option value="5">Budha</option>
                            <option value="6">Konghucu</option>

                        </select>
                        <label>Agama</label>
                    </div>
                    <div class="input-field col s4 left">
                        <select>
                            <option value="" disabled selected>Pilih Grant Akses</option>
                            <option value="1">HRD</option>
                            <option value="2">Admin</option>
                            <option value="3">Others</option>
                        </select>
                        <label>Grant Access</label>
                    </div>
                </div>
                

                <div class="row">
                    <label class="active">Upload Image</label>
                    <div class="file-field input-field col s12">

                        <div class="btn teal lighten-1">
                            <span>File</span>
                            <input type="file">
                        </div>
                        <div class="file-path-wrapper">
                            <input class="file-path validate" type="text">
                        </div>
                    </div>
                </div>
               
                
                <span><input type="button" name="reset" value="Cancel" class="modal-action modal-close waves-effect waves-light btn red"></span>
                <span><input type="submit" name="submit" value="Sumbit" class="waves-effect waves-light btn green" ></span>
            </form>
        </div>
    </div>
</main>
<div class="clearfix"></div>

<?php $this->load->view('admin/footer'); ?>            