<?php $this->load->view('admin/header'); ?>
<main class="mn-inner">
            <div class="row">

                <div class="col s12 m4 l3">
                    <div class="card">
                        <div class="card-content center-align">
                            <img src="<?php echo base_url() ?>assets/images/profile-image-2.png" class="responsive-img circle" width="128px" alt="">
                            <p class="m-t-lg flow-text">Desmon William</p>
                            <div class="chip m-t-sm blue-grey white-text">UX/UI Designer</div>
                            
                        </div>
                    </div>

                    
                </div>
                <div class="col s12 m4 l9">
                    <div class="card">
                        <div class="card-content ">
                        <div class="right-align">Status : <div class=" chip m-t-sm blue white-text" data-id="badge">New</div> <span id="print" style="display:none"><a href="printout-cuti.html" class="waves-effect waves-grey btn-flat "><i class="material-icons">print</i></a></span></div>
                            <div class="row">
                              <div class="col s3 no-padding">
                                <label>Supervisor</label>
                                <input type="text" name="" disabled value="Intan Gustiarti" class="rounded">
                              </div>
                            </div>
                            <div class="row">
                              <div class="col s3 no-padding" style="margin-right:5%">
                                <label>Date Submit</label>
                                <input type="text" name="" disabled value="20/12/2016" class="rounded">
                              </div>
                              <div class="col s3 no-padding">
                                <label>Date CA</label>
                                <input type="text" name="" disabled value="20/12/2016" class="rounded">
                              </div>
                            </div>

                            <div class="row">
                              <div class="col s3 no-padding" style="margin-right:5%">
                                <label>Projects</label>
                                <input type="text" name="" disabled value="TCM" class="rounded">
                              </div>
                              <div class="col s3 no-padding">
                                <label>Amount CA</label>
                                <input type="text" name="" disabled value="Rp 200.000" class="rounded">
                              </div>
                            </div>

                            <div class="row">
                              <div class="col s6 no-padding">
                                <textarea class="materialize-textarea" disabled>Voucher Xl 100</textarea>
                              </div>
                            </div>
                            

                          <ul class="nav-icon-action center-align">
                                <li id="approval"><a href="javascript:;" data-id="approved" title="approve" class="waves-effect waves-grey btn green white-text ">Terima</a></li>
                                <li id="rejected"><a href="javascript:;" title="tolak" class="waves-effect waves-grey btn-flat ">
                                    Tolak</a></li>
                                
                            </ul>
                        </div>
                        
                        
                    </div>
                </div>

            </div>
</main>


    
<div class="clearfix"></div>
<?php $this->load->view('admin/footer'); ?>   