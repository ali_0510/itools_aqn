<!DOCTYPE html>
<html lang="en">
    <head>
        
        <!-- Title -->
        <title>AQN | Asia Quatro Network</title>
        
        <!-- <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/> -->
        <meta name="viewport" content='width=1190'>
        <meta charset="UTF-8">
        <meta name="description" content="Responsive Admin Dashboard Template" />
        <meta name="keywords" content="admin,dashboard" />
        <meta name="author" content="Steelcoders" />
        
        <!-- Styles -->
        <link type="text/css" rel="stylesheet" href="<?php echo base_url();?>assets/plugins/materialize/css/materialize.min.css"/>
        <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/datetimepicker.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/select2.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/ui-custom.css">
        <!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/jquery-ui.theme.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/jquery-ui.structure.css"> -->
        
        <link href="<?php echo base_url();?>assets/plugins/material-preloader/css/materialPreloader.min.css" rel="stylesheet">        
        <link href="<?php echo base_url();?>assets/plugins/sweetalert/sweetalert.css" rel="stylesheet" type="text/css"/> 
        <link href="<?php echo base_url();?>assets/plugins/datatables/css/jquery.dataTables.min.css" rel="stylesheet">
        <link href="<?php echo base_url();?>assets/css/bootstrap-editable.css" rel="stylesheet"/>
        <!-- Theme Styles -->
        <link href="<?php echo base_url();?>assets/css/alpha.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url();?>assets/css/custom.css" rel="stylesheet" type="text/css"/>
        
        
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="http://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="http://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        
    </head>
    <body>
        <div class="loader-bg"></div>
        <div class="loader">
            <div class="preloader-wrapper big active">
                <div class="spinner-layer spinner-blue">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div><div class="gap-patch">
                    <div class="circle"></div>
                    </div><div class="circle-clipper right">
                    <div class="circle"></div>
                    </div>
                </div>
                <div class="spinner-layer spinner-spinner-teal lighten-1">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div><div class="gap-patch">
                    <div class="circle"></div>
                    </div><div class="circle-clipper right">
                    <div class="circle"></div>
                    </div>
                </div>
                <div class="spinner-layer spinner-yellow">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div><div class="gap-patch">
                    <div class="circle"></div>
                    </div><div class="circle-clipper right">
                    <div class="circle"></div>
                    </div>
                </div>
                <div class="spinner-layer spinner-green">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div><div class="gap-patch">
                    <div class="circle"></div>
                    </div><div class="circle-clipper right">
                    <div class="circle"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="mn-content fixed-sidebar">
         <header class="mn-header navbar-fixed">
            <nav class="grey darken-1">
                <div class="container">
                    <div class="nav-wrapper row" style="padding:0">
                                
                        <div class="col s12">
                            
                            <ul class="menu-header-network">
                                <li class="<?php if($this->uri->uri_string() == 'system/userpanel') { echo 'active'; } ?>"><a href="<?php echo site_url();?>/system/userpanel"><i class="material-icons">home</i> 
                                    Home</a></li>                                
                                <li class="<?php if($this->uri->uri_string() == 'user/myAccount') { echo 'active'; } ?>"><a href="javascript:;" class="dropthis"> <i class="material-icons">account_circle</i> User</a></li>
                                <li class="<?php if($this->uri->uri_string() == 'user/history') { echo 'active'; } ?>"><a href="<?php echo site_url() ?>/user/history"><i class="material-icons">history</i>History</a></li>
                                <!-- <li class="<?php if($this->uri->uri_string() == 'user/settlement') { echo 'active'; } ?>"><a href="<?php echo site_url() ?>/user/settlement"><i class="material-icons">local_atm</i>Settlement</a></li> -->
                                <li ><a href=""><i class="material-icons">assessment</i>Reimbursement</a>
                                    <ul>
                                        <li class="<?php if($this->uri->uri_string() == 'user/reimbursementproj') { echo 'active'; } ?>"><a href="<?php echo site_url() ?>/user/reimbursementproj">Reimbursement Project</a></li>
                                        <li class="<?php if($this->uri->uri_string() == 'user/reimbursementnonproj') { echo 'active'; } ?>"><a href="<?php echo site_url() ?>/user/reimbursementnonproj">Reimbursement Non Project</a></li>
                                        <li class="<?php if($this->uri->uri_string() == 'user/reimbursemententertain') { echo 'active'; } ?>"><a href="<?php echo site_url() ?>/user/reimbursemententertain">Reimbursement Entertainment</a></li>
                                    </ul>
                                </li> 
                                <li class="<?php if($this->uri->uri_string() == 'user/cashAdvance') { echo 'active'; } ?>"><a href="<?php echo site_url() ?>/user/cashAdvance"><i class="material-icons">local_atm</i>Cash Advance</a></li> 
                                
                                <li class="<?php if($this->uri->uri_string() == 'user/annual') { echo 'active'; } ?>"> <a href="<?php echo site_url() ?>/user/annual"><i class="material-icons">assignment</i> Annual Leave</a></li>

                            </ul>
                        </div>

                        <ul id='dropdown2' class='content-drop'>
                            <li class="<?php if($this->uri->uri_string() == 'user/myAccount') { echo 'active'; } ?>"><a href="<?php echo site_url();?>/user/myAccount">My Account</a></li>
                            <li class="divider"></li>
                            <li><a href="<?php echo base_url();?>">Logout</a></li>
                        </ul>


                    </div>
                   
                </div>
            </nav>
        </header>