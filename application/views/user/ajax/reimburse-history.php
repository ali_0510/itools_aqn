<table id="example2" class="display datatable-example responsive-table highlight striped">
    <thead>
        <tr>
            <th>No</th>
            <th>Reimburse Type</th>
            <th>Date Submit</th>
            <th>Date of Bill</th>
            <th>Type</th>
            <th width="250">Description</th>
            <th>Amount</th>
            <th>Status</th>
            <th>Print</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>1</td>
            <td>Reimburse Project</td>
            <td>11/11/2016</td>
            <td>14/11/2016</td>
            <td>Voucher Telkomsel</td>
            <td>Lorem ipsum dolor sit amet, consectetur adipisicing elit. </td>
            <td>$320,800</td>
            <td><div class="badge-status"><span class="badge green">Diterima</span></div></td>
            <td><a href="<?php echo site_url() ?>/user/printoutReimbproj"><i class="material-icons">print</i></a></td>
        </tr>
        <tr>
            <td>2</td>
            <td>Reimburse Entertain</td>
            <td>15/11/2016</td>
            <td>19/11/2016</td>
            <td>Dinner</td>
            <td>Ut enim ad minim veniam, quis nostrud exercitation ullamco.</td>
            <td>$170,750</td>
            <td><div class="badge-status"><span class="badge green">Diterima</span></div></td>
            <td><a href="<?php echo site_url() ?>/user/printoutReimbent"><i class="material-icons">print</i></a></td>
        </tr>
        <tr>
            <td>3</td>
            <td>Reimburse Non Project</td>
            <td>16/11/2016</td>
            <td>20/11/2016</td>
            <td>Pulse</td>
            <td>Ut enim ad minim veniam, quis nostrud exercitation ullamco.</td>
            <td>$170,750</td>
            <td><div class="badge-status"><span class="badge green">Diterima</span></div></td>
            <td><a href="<?php echo site_url() ?>/User/printoutReimb"><i class="material-icons">print</i></a></td>
        </tr>
       
    </tbody>
</table>


 
