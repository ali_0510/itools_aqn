<table id="example1" class="display datatable-example responsive-table highlight striped">
        <thead>
        <tr>
            <th>No</th>
            <th>Nama</th>
            <th>Particulars</th>
            <th>Tgl Pengajuan</th>
            <th>Periode Cuti</th>
            <th>Jumlah Cuti</th>
            <th>Pengganti Cuti</th>
            <th>Keterangan Cuti</th>
            <th>Status</th>
            <th>Print</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>1</td>
            <td>Andi</td>
            <td>Annual Leave</td>
            <td>11/11/2016</td>
            <td>12/11/2016 - 20/11/2016</td>
            <td>3 Hari</td>
            <td>Megawati</td>
            <td>Lorem Ipsum Dolor Siat Amet</td>
            <td><div class="badge-status"><span class="badge green">Diterima</span></div></td>
            <td><a href="<?php echo site_url() ?>/User/printoutCuti"><i class="material-icons">print</i></a></td>
        </tr>
        <tr>
            <td>2</td>
            <td>Imam</td>
            <td>Annual Leave</td>
            <td>11/12/2016</td>
            <td>12/12/2016 - 20/12/2016</td>
            <td>8 Hari</td>
            <td>Ivan</td>
            <td>Lorem Ipsum Dolor Siat Amet</td>
            <td><div class="badge-status"><span class="badge red">Ditolak</span></div></td>
            <td><a href=""><i class="material-icons">print</i></a></td>
        </tr>
        
    </tbody>
</table>
