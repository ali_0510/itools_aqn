<table id="example2" class="display datatable-example responsive-table highlight striped">
    <thead>
        <tr>
            <th>No</th>
            <th>Name</th>
            <th>Date Submit</th>            
            <th>CA Ammount</th>
            <th>Projects Code</th>
            <th>CA Code</th>
            <th>Amount</th>
            <th width="250">Description</th>            
            <th>Settlement</th>
            <th>Print</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>1</td>
            <td>Desmon William</td>
            <td>11/11/2016</td>
            <td>Rp 10.000.000</td>
            <td>PJ0002</td>
            <td>CA0002</td>
            <td>Rp 7.500.000</td>
            <td>1 unit Lenovo E550</td>            
            <td>Rp 2.500.000</td>
            <td><a href="<?php echo site_url() ?>/User/printoutSettlement"><i class="material-icons">print</i></a></td>
        </tr>
        <tr>
            <td>2</td>
            <td>Desmon William</td>
            <td>16/11/2016</td>
            <td>Rp 10.000.000</td>
            <td>PJ0003</td>
            <td>CA0003</td>
            <td>Rp 8.000.000</td>            
            <td>1 unit Lenovo E450</td>            
            <td>Rp 2.000.000 </td>
            <td><a href="<?php echo site_url() ?>/User/printoutSettlement"><i class="material-icons">print</i></a></td>
        </tr>
       
    </tbody>
</table>


 
