<!DOCTYPE html>
<html lang="en">
    <head>
        
        <!-- Title -->
        <title>AQN | Asia Quattro Network</title>
        
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
        <meta charset="UTF-8">
        <meta name="description" content="Responsive Admin Dashboard Template" />
        <meta name="keywords" content="admin,dashboard" />
        <meta name="author" content="Steelcoders" />
        
       <!-- Styles -->
        <link type="text/css" rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/materialize/css/materialize.min.css"/>
        <link href="<?php echo base_url() ?>assets/plugins/material-preloader/css/materialPreloader.min.css" rel="stylesheet">        
        <link href="<?php echo base_url() ?>assets/css/print.css" rel="stylesheet" type="text/css"/>
            
        <!-- Theme Styles -->
        <link href="<?php echo base_url() ?>assets/css/alpha.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url() ?>assets/css/custom.css" rel="stylesheet" type="text/css" media="print"/>
        
        
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="http://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="http://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        
    </head>
    <body>
        <div class="page">
           <img src="<?php echo base_url() ?>assets/images/logo_aqn.png" width="200" class="left"> 
           <h6 style="margin:20px 0 0" class="right"><strong>CASH ADVANCE FORM</strong></h6>
            
            <ul class="detail-list-2">
                <li><span class="title">Nama</span><span class="value_name">Desmon William</span></li>
                <li><span class="title">Jabatan</span><span class="value_name">UX Designer</span></li>
                <li><span class="title">Amount CA</span><span class="value_name">Rp 15.000.000</span></li>
            </ul>
            <br/>
            <br/>

            <table id="example2" class="responsive-table highlight striped bordered">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Date Submit</th>
                        <th>Date of Bill</th>
                        <th>Projects</th>
                        <th width="200">Description</th>
                        <th>Amount</th>
                        
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1</td>
                        <td>03/11/2016</td>
                        <td>11/11/2016</td>
                        <td>Kineria</td>
                        <td>Lorem ipsum dolor sit amet, consectetur adipisicing elit. </td>
                        <td>Rp 320,800</td>
                        
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>06/11/2016</td>
                        <td>10/11/2016</td>
                        <td>Telkomsel</td>
                        <td>Ut enim ad minim veniam, quis nostrud exercitation ullamco.</td>
                        <td>Rp 170,750</td>
                        
                    </tr>
                   
                </tbody>
            </table>
            
            <br/>
            <br/>

            <div class="left">
                <p class="center-align">Submitted By</p>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
               
                <p class="center-align">(Desmon William)</p>
            </div>
                   
            <div class="right">
                <p class="center-align">Approved By</p>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
               
                <p class="center-align">(Suliana Widjaja)</p>
            </div>
        </div>        
        <!-- Javascripts -->
        <script src="<?php echo base_url() ?>assets/plugins/jquery/jquery-2.2.0.min.js"></script>
        <script src="<?php echo base_url() ?>assets/plugins/materialize/js/materialize.min.js"></script>
        <script src="<?php echo base_url() ?>assets/js/alpha.min.js"></script>
        <script type="text/javascript">
            window.print();
        </script>
        
    </body>
</html>