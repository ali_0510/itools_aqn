
<?php $this->load->view('user/header'); ?>
<main class="mn-inner">
<div class="row">
    <div class="col s12">
        <h5>History</h5>
        <hr/>
        <div class="row">
            <ul id="navAjax">
              <li><a href="<?php echo site_url();?>/User/ajxAnnual/" class="<?php if($this->uri->uri_string() == 'User/ajxAnnual') { echo 'current'; } ?>">History Cuti</a></li>
              <li class="<?php if($this->uri->uri_string() == 'User/ajxReimb') { echo 'current'; } ?>"><a href="<?php echo site_url();?>/User/ajxReimb/">History Reimburse</a></li>
              <li class="<?php if($this->uri->uri_string() == 'User/ajxCa') { echo 'current'; } ?>"><a href="<?php echo site_url();?>/User/ajxCa/">History Cash Advance</a></li>
              <li class="<?php if($this->uri->uri_string() == 'User/ajxSt') { echo 'current'; } ?>"><a href="<?php echo site_url();?>/User/ajxSt/">History Settlement</a></li>
            </ul>

            <div id="ajax-content"></div>
        </div>
        
    </div>
</div>
</main>
<?php $this->load->view('user/footer'); ?>

          