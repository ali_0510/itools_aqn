<!DOCTYPE html>
<html lang="en">
    <head>
        
        <!-- Title -->
        <title>AQN | Asia Quattro Network</title>
        
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
        <meta charset="UTF-8">
        <meta name="description" content="Responsive Admin Dashboard Template" />
        <meta name="keywords" content="admin,dashboard" />
        <meta name="author" content="Steelcoders" />
        
       <!-- Styles -->
        <link type="text/css" rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/materialize/css/materialize.min.css"/>
        <link href="<?php echo base_url() ?>assets/plugins/material-preloader/css/materialPreloader.min.css" rel="stylesheet">        
        <link href="<?php echo base_url() ?>assets/css/print.css" rel="stylesheet" type="text/css"/>
            
        <!-- Theme Styles -->
        <link href="<?php echo base_url() ?>assets/css/alpha.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url() ?>assets/css/custom.css" rel="stylesheet" type="text/css" media="print"/>
        
        
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="http://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="http://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style type="text/css" media="print">
        .landscape { 
            width: 100%; 
            height: 100%; 
            margin: 0% 0% 0% 0%; filter: progid:DXImageTransform.Microsoft.BasicImage(Rotation=1); 
        } 
        .landscape table.table-border{
            width: 120%;
            margin: 0 -65px;
        }
        </style>
    </head>
    <body>
        <div class="page landscape">

                <img src="<?php echo base_url() ?>assets/images/logo_aqn.png" width="200" class="left"> 
                <h6 style="margin:20px 0 0" class="right"><strong>REIMBURSEMENT ENTERTAIN FORM</strong></h6>
            
            <ul class="detail-list-2">
                <li><span class="title">Date Submit</span><span class="value_name">11/11/2016</span></li>
                <li><span class="title">Nama</span><span class="value_name">Desmon William</span></li>
                <li><span class="title">Jabatan</span><span class="value_name">UX Designer</span></li>
                
            </ul>
           
            <table class="responsive-table bordered table-border">
              <tr>
                <td rowspan="2">No</td>
                <th colspan="2" scope="colgroup" width="300">Pemberian Entertainment</th>
                <th colspan="3" scope="colgroup" width="120">Relasi Usaha Entertainment</th>
                <th rowspan="2" colspan="1" scope="colgroup">Project Description</th>
                <th rowspan="2" colspan="1" scope="colgroup">Keterangan</th>
              </tr>
              <tr>
                <th scope="col">Tanggal</th>
                <th scope="col">Jumlah(Rp)</th>
                <th scope="col">Nama</th>
                <th scope="col">Posisi</th>
                <th scope="col">Nama Perusahaan</th>
                
              </tr>
              
              <tr>
                <th scope="row">1</th>
                <td>20-Dec-2015</td>
                <td>100,000</td>
                <td>Dimas, Aditya, Hermansyah</td>
                <td>Self Care, CCM</td>
                <td>Telkomsel</td>
                <td>TCM</td>
                <td>Desmon</td>
              </tr>
              <tr>
                <th scope="row">2</th>
                <td>25-Dec-2016</td>                                
                <td>120,000</td>
                <td>Dimas, Aditya, Hermansyah</td>
                <td>Self Care, CCM</td>
                <td>XL</td>
                <td>GEO</td>
                 <td>Desmon</td>
              </tr>
            </table>
  
            <br/>
            <br/>
            <div class="left">
                <p class="center-align">Submitted By</p>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
               
                <p class="center-align">(Desmon William)</p>
            </div>
                   
            <div class="right">
                <p class="center-align">Approved By</p>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
               
                <p class="center-align">(Suliana Widjaja)</p>
            </div>
        
        </div>        
        <!-- Javascripts -->
        <script src="<?php echo base_url() ?>assets/plugins/jquery/jquery-2.2.0.min.js"></script>
        <script src="<?php echo base_url() ?>assets/plugins/materialize/js/materialize.min.js"></script>
        <script src="<?php echo base_url() ?>assets/js/alpha.min.js"></script>
        <script type="text/javascript">
            window.print();
        </script>
        
    </body>
</html>