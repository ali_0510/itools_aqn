<!DOCTYPE html>
<html lang="en">
    <head>
        
        <!-- Title -->
        <title>AQN | Asia Quattro Network</title>
        
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
        <meta charset="UTF-8">
        <meta name="description" content="Responsive Admin Dashboard Template" />
        <meta name="keywords" content="admin,dashboard" />
        <meta name="author" content="Steelcoders" />
        
        <!-- Styles -->
        <link type="text/css" rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/materialize/css/materialize.min.css"/>
        <link href="<?php echo base_url() ?>assets/plugins/material-preloader/css/materialPreloader.min.css" rel="stylesheet">        
        <link href="<?php echo base_url() ?>assets/css/print.css" rel="stylesheet" type="text/css"/>
            
        <!-- Theme Styles -->
        <link href="<?php echo base_url() ?>assets/css/alpha.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url() ?>assets/css/custom.css" rel="stylesheet" type="text/css" media="print"/>
        
        
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="http://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="http://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        
    </head>
    <body>
        <div class="page">
            <!-- <div class="right-align">
                <img src="<?php echo base_url() ?>assets/images/logo_aqn.png" width="200">
                <div class="separate-div" style="border-color: #ff9934;"></div>                
            </div> -->
             <h6 style="margin:20px 0"><strong>LEAVE APPLICATION</strong></h6>
                
            <div class="frame-form"><div class="container-form">
               
                <ul class="detail-list">
                    <li><span class="title">Name</span><span class="value_name">Ahmad Suhaili</span></li>
                    <li><span class="title">Jabatan</span><span class="value_name">Front End Developer</span></li>
                    <li><span class="title">Particulars</span><span class="value_name">Annual Leave</span></li>
                    <li><span class="title">Leave Will Be Applied From</span><span class="value_name">11/12/2016 to 15/12/2016</span></li>
                    <li><span class="title">Total Number Of Days</span><span class="value_name">3 Days</span></li>
                    <li><span class="title">Substitute During Absence</span><span class="value_name">Alex</span></li>
                    <li><span class="title">Contact Address &amp; Tel</span><span class="value_name">Jl. Gaharu I no 5 087786407337</span></li>
                    <li><span class="title">Explanation(IF ANY)</span><span class="value_name">-</span></li>
                    <li><span class="title">Leave Balance 2015</span><span class="value_name">0</span></li>
                    <li><span class="title">Leave Balance 2016</span><span class="value_name">6</span></li>
                    <li><span class="title">Comments &amp; Decisions</span><br/><div>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua ut labore et dolore magnau</div></li>
                    
                </ul>
                
            
            </div></div>

            <div class="signature left">
                <p style="text-align:center">Applicant's Signature</p>
                <div class="content-signature"></div> 
            </div>
                   
            <div class="signature right">
                <p style="text-align:center">Approved By</p>
                <div class="content-signature"></div>
            </div>

            <div class="clearfix"></div>

            
        </div>        
        <!-- Javascripts -->
        <script src="<?php echo base_url() ?>assets/plugins/jquery/jquery-2.2.0.min.js"></script>
        <script src="<?php echo base_url() ?>assets/plugins/materialize/js/materialize.min.js"></script>
        <script src="<?php echo base_url() ?>assets/js/alpha.min.js"></script>
        <script type="text/javascript">
            window.print();
        </script>
        
    </body>
</html>