<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->load->view('user/header'); ?>
<main class="mn-inner" style="padding:0 80px">
    <div class="row">
        <div class="col s12">
            <h5>Registrasi Cuti</h5>
            <hr/>
           
            <form class="form-custom" action="<?php echo site_url();?>/System/userpanel">
                <div class="col s6">
                    <div class="row">
                        <div class="input-field  col s6 no-padding">
                            <input id="applicant" type="text" class="validate" disabled value="Desmon">
                            <label for="applicant" class="active">Nama</label>
                        </div>                               
                    </div>
                    <div class="row">
                        <div class="input-field no-padding">
                            
                            <select class="browser-default" id="annual">
                                <option value="" disabled selected>Choose your option</option>
                                <option value="1">Annual Leave</option>
                                <option value="2">Unpaid Leave</option>
                                <option value="3">Special Leave</option>
                            </select>
                            <label class="active" style="top:0">Particulars</label>
                            
                        </div>                  
                    </div>

                    <div class="row" id="special-leave" style="display:none;margin-top:10px;">
                        <div class="input-field no-padding">
                            
                            <select class="browser-default">
                                <option value="" disabled selected>Choose your option</option>
                                <option value="2">Cuti Hamil (3 Bulan)</option>
                                <option value="3">Cuti Menikah (3 Hari)</option>
                                <option value="4">Menikahkan Anak (2 Hari)</option>
                                <option value="5">Mengkhitankan Anak (2 Hari)</option>
                                <option value="6">Membaptis Anak (2 Hari)</option>
                                <option value="7">Istri Melahirkan atau Keguguran (2 Hari)</option>
                                <option value="8">Suami / Istri , Mertua atau Anak atau Menantu Meninggal (2 Hari)</option>
                                <option value="9">Anggota Keluarga Meninggal (1 Hari)</option>
                            </select>
                            
                        </div>                  
                    </div>
                    
                    <div class="row">
                        <div class="input-field col s5 no-padding" style="margin-right:84px;">
                            <label class="active" style="position:relative">Tanggal Cuti</label>
                            <div id="simpliest-usage" class="box" style="margin-top:20px;"></div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="input-field col s5 no-padding" style="margin-right:84px;">
                            <input id="totalofdays" type="text" class="validate" >
                            <label for="totalofdays" class="active">Jumlah Cuti</label>
                        </div>
                        <div class="input-field col s5 no-padding">
                            <input id="substitution" type="text" class="validate" >
                            <label for="substitution" class="active">Pengganti Saat Cuti</label>
                        </div>                               
                    </div>

                    <div class="row">
                        <div class="col s12 no-padding" style="margin-right:5%">
                            <label class="active" style="top:0">Supervisor</label>                       
                            <select class="browser-default" style="margin-top:12px;">
                                <option>Saut M</option>
                                <option>Priyadi Budi</option>
                                <option>Ade Kurnia</option>
                            </select>
                        </div>
                    </div>

                    <div class="row">
                        <div class="input-field  no-padding">
                            <textarea id="textarea2" class="materialize-textarea" length="160"></textarea>
                            <label for="textarea2" class="active">Info Detail Pengganti</label>
                        </div>                               
                    </div>

                    <div class="row">
                        <div class="input-field  no-padding">
                            <textarea id="textarea1" class="materialize-textarea" length="160"></textarea>
                            <label for="textarea1" class="active">Keterangan</label>
                        </div>
                    </div>
                     <span><input type="button" name="reset" value="Reset" class="waves-effect waves-light btn red"></span>
                     <span><input type="submit" name="submit" value="Ajukan" class="waves-effect waves-light btn green 
                     sweetalert-success" ></span>
                </div>

                
                <div class="col s4 right">
                    <ul class="collection">
                      <li class="collection-item"><span class="cl_name">Remaining Balance</span><span class="cl_value">8 Days</span></li>
                      <li class="collection-item"><span class="cl_name">Leave Entitlement</span><span class="cl_value">5 Days</span></li>
                      <li class="collection-item"><span class="cl_name">Leave Already Taken in</span><span class="cl_value">4 Days</span></li>
                      
                    </ul>
                </div>

            </form>
        </div>
    </div>
</main>
<div class="clearfix"></div>
<?php $this->load->view('user/footer'); ?>
