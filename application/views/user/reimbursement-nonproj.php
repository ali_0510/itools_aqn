
<?php $this->load->view('user/header'); ?>

<main class="mn-inner" style="padding:0 80px">
    <div class="row">
        <div class="col s12">
            <h5>Pengajuan Reimburse</h5>
            <hr/>
          
            <form class="form-custom" action="">
            	<div class="row">
            		<div class="col s1 no-padding">
            			<label>Date Submited</label>
            			<input type="text" name="" disabled value="11-11-2016">
            		</div>                    
            	</div>
                <div class="row">
                    <div class="col s3 no-padding">
                        <label>Nama</label>
                        <input type="text" name="" disabled value="Desmon">
                    </div>
                </div>
                <div class="row">
                    <div class="col s3 no-padding" style="margin-right:5%">
                        <label class="active" style="top:0">Supervisor</label>                       
                        <select class="browser-default" style="margin-top:12px;">
                            <option>Saut M</option>
                            <option>Priyadi Budi</option>
                            <option>Ade Kurnia</option>
                        </select>

                    </div>
                </div>
                <table class="display datatable-example responsive-table highlight striped" id="editableRow">
                	 	<thead>
                        <tr>
                        	<th>No</th>
                            <th width="100">Date Of Bill</th>
                            <th width="250">Type</th>
                            <th width="250">Description</th>
                            <th width="250">Amount</th>
                            <th>Action</th>
                        </tr>
                    	</thead>
	                    <tfoot>
	                        <tr>
	                        	<th>No</th>	                          
	                            <th width="100">Date Of Bill</th>
	                            <th width="250">Type</th>
                            	<th width="250">Description</th>
                            	<th width="250">Amount</th>
                            	<th>Action</th>
	                        </tr>
	                    </tfoot>
	                    <tbody>
	                        <tr>
	                        	<td id="number">1</td>
	                            <td><a href="javascript:;" id="datetaken" data-type="combodate" data-format="YYYY-MM-DD" data-viewformat="DD/MM/YYYY" data-template="D / MMM / YYYY" data-pk="1"  data-title="Select Date of Bill">null</a></td>
	                            <td><a href="javascript:;" id="typeOffice" data-type="select2" data-pk="1" data-title="Select Type">null</a></td>
	                            <td><a href="javascript:;" data-type="textarea" data-placeholder="Your Descriptions here..." data-pk="5" class="myeditable">null</a></td>
	                            <td><a href="javascript:;" data-currency="amount" data-pk="6" class="myeditable">null</a></td>
                                <td><button type="button" data-id="addrow-reimburse" id="save-btn" class="waves-effect waves-light btn green" style="margin-right:10px;">Save</button><button type="button" id="delete-btn" class="waves-effect waves-light btn red">Delete</button></td>
	                        </tr>

	                        
	                   </tbody>
                </table>

                <span><input type="button" name="reset" value="Reset" class="waves-effect waves-light btn red"></span>
                <span><input type="submit" name="submit" value="Ajukan" class="waves-effect waves-light btn green sweetalert-success" ></span>
            </form>
        </div>
    </div>
</main>
<div class="clearfix"></div>

<?php $this->load->view('user/footer'); ?>

