<!DOCTYPE html>
<html lang="en">
    <head>
        
        <!-- Title -->
        <title>AQN | Asia Quattro Network</title>
        
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
        <meta charset="UTF-8">
        <meta name="description" content="Responsive Admin Dashboard Template" />
        <meta name="keywords" content="admin,dashboard" />
        <meta name="author" content="Steelcoders" />
        
       <!-- Styles -->
        <link type="text/css" rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/materialize/css/materialize.min.css"/>
        <link href="<?php echo base_url() ?>assets/plugins/material-preloader/css/materialPreloader.min.css" rel="stylesheet">        
        <link href="<?php echo base_url() ?>assets/css/print.css" rel="stylesheet" type="text/css"/>
            
        <!-- Theme Styles -->
        <link href="<?php echo base_url() ?>assets/css/alpha.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url() ?>assets/css/custom.css" rel="stylesheet" type="text/css" media="print"/>
        
        
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="http://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="http://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        
    </head>
    <body>
        <div class="page">
            <img src="<?php echo base_url() ?>assets/images/logo_aqn.png" width="200" class="left"> 
            <h6 style="margin:20px 0 0" class="right"><strong>CASH ADVANCE &amp; SETTLEMENT FORM</strong></h6>
           
            <ul class="detail-list-2">
                <li><span class="title">Date Submit</span><span class="value_name">12/12/2016</span></li>
                <li><span class="title">CA Code</span><span class="value_name">CA201611001</span></li>
                <li><span class="title">Nama</span><span class="value_name">Desmon William</span></li>
                <li><span class="title">Jabatan</span><span class="value_name">UX Designer</span></li>
                <li><span class="title">Project Name</span><span class="value_name">Telkomsel</span></li>
                
            </ul>

            <table class="responsive-table bordered table-border">
            
              <tr>
                <td colspan="1" width="130" style="background:#ddd"></td>
                <th colspan="1" scope="colgroup" width="120" style="text-align:center">Date</th>
                <th colspan="1" scope="colgroup" width="110" style="text-align:center">Amount (Rp)</th>
                <th colspan="3" scope="colgroup" width="150" style="text-align:center">Notes</th>
                <th colspan="2" scope="colgroup" width="120" style="text-align:center">Signature</th>
              </tr>
              
              <tr height="90">
                <th scope="row" width="130">Cash Advance</th>
                <td colspan="1">12/12/2016</td>
                <td colspan="1">Rp 10.000.000</td>
                <td colspan="3">Lorem ipsum dolor sit amet fusce</td>
                <td colspan="2"></td>
                
              </tr>
              <tr height="90">
                <th scope="row" width="130">Settlement</th>
                <td colspan="1">12/12/2016</td>
                <td colspan="1">Rp 8.000.000</td>
                <td colspan="3">Lorem ipsum dolor sit amet fusce</td>
                <td colspan="2"></td>
                
              </tr>
            </table>
            
            <!-- <table id="example2" class="responsive-table highlight striped bordered">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Date</th>
                        <th>Projects</th>
                        <th width="200">Description</th>
                        <th>Amount</th>
                        
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1</td>
                        <td>03/11/2016</td>
                        <td>PJ0002</td>
                        <td>Lorem ipsum dolor sit amet, consectetur adipisicing elit. </td>
                        <td>Rp 6.000.000</td>
                        
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>06/11/2016</td>
                        <td>PJ0003</td>
                        <td>Ut enim ad minim veniam, quis nostrud exercitation ullamco.</td>
                        <td>Rp 2.000.000</td>
                        
                    </tr>
                   

                    <tr>
                        
                        <td colspan="4"  style="text-align:right"><span>Settlement :</span></td>
                        <td colspan="1"><span>Rp 2.000.000</span></td>
                        
                    </tr>

                    <tr>
                        
                        <td colspan="4"  style="text-align:right"><span>Settlement :</span></td>
                        <td colspan="1"><span>(Rp 2.000.000)</span></td>
                        
                    </tr>
                   
                </tbody>
            </table> -->
            
      

            <br>
            <br>
            <br>

            <div class="left">
                <p class="center-align">Submitted By</p>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
               
                <p class="center-align">(Desmon William)</p>
            </div>
                   
            <div class="right">
                <p class="center-align">Approved By</p>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
               
                <p class="center-align">(Suliana Widjaja)</p>
            </div>

            
        </div>        
        <!-- Javascripts -->
        <script src="<?php echo base_url() ?>assets/plugins/jquery/jquery-2.2.0.min.js"></script>
        <script src="<?php echo base_url() ?>assets/plugins/materialize/js/materialize.min.js"></script>
        <script src="<?php echo base_url() ?>assets/js/alpha.min.js"></script>
        <script type="text/javascript">
            window.print();
        </script>
        
    </body>
</html>