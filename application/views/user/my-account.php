
<?php $this->load->view('user/header'); ?>
<main class="mn-inner">
<div class="row">

        <div class="col s12 m4 l3">
            <div class="card">
                <div class="card-content center-align">
                    <img src="<?php echo base_url() ?>assets/images/profile-image-2.png" class="responsive-img circle" width="128px" alt="">
                    <p class="m-t-lg flow-text">John Doe</p>
                    <div class="chip m-t-sm blue-grey white-text">UI Designer</div> 
                    
                </div>
            </div>
        
        </div>
        <div class="col s12 m4 l9">
            
                    <ul class="collection" style="border:none">
                      <li class="collection-item"><span class="cl_name">Nama</span><span class="cl_value">John Doe</span></li>
                      <li class="collection-item"><span class="cl_name">NIK</span><span class="cl_value">PG00001</span></li>
                      <li class="collection-item"><span class="cl_name">No. KTP</span><span class="cl_value">6928002939293828</span></li>
                      <li class="collection-item"><span class="cl_name">Telepon</span><span class="cl_value">087766554221</span></li>
                      <li class="collection-item"><span class="cl_name">Jenis Kelamin</span><span class="cl_value">Laki-Laki</span></li>
                      <li class="collection-item"><span class="cl_name">Agama</span><span class="cl_value">Islam</span></li>
                      <li class="collection-item"><span class="cl_name">Status</span><span class="cl_value">Menikah</span></li>
                      <li class="collection-item"><span class="cl_name">Alamat</span><span class="cl_value">Jl. Raya Citayam RT 002/002 No. 49B  Pondok Terong Cipayung Depok JawaBarat </span></li>
                  </ul>
                
        </div>
        
    </div>

    
</main>
<?php $this->load->view('user/footer'); ?>
            
          