<?php $this->load->view('finance/header'); ?>   
<main class="mn-inner" style="padding:0 80px">
    <div class="row">
        <div class="col s12">
            <h5>Reimburse Data Employee</h5>
            <hr/>
            <table id="example" class="display responsive-table datatable-example bordered">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>Date Submit</th>
                        <th>Reimburse Status</th>
                        <th width="250">Description</th>
                        <th>Status</th>
                        <th width="150">Action</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>Date Submit</th>
                        <th>Reimburse Status</th>
                        <th width="250">Description</th>
                        <th>Status</th>
                        <th width="150">Action</th>
                    </tr>
                </tfoot>
                <tbody>
                    <tr>
                        <td>1</td>
                        <td>Aji Notomo</td>
                        <td>11/11/2016</td>
                        <td>Project</td>                        
                        <td>Kemaren beli voucher tapi g ada struknya</td>
                                                           
                        <td>
                            <ul class="nav-icon-action">
                                <li><div class="badge-status"><span class="badge blue" data-id="badge">New</span></div></li>
                            </ul>
                        </td>
                        <td>
                            <ul class="nav-icon-action">
                                <li><a href="<?php echo site_url() ?>/admin_finance/viewDetaildatareimbproj" title="view detail" class="btn white-text">Detail</a></li>
                                
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>Desmon</td>
                        <td>08/11/2016</td>
                        <td>Non Project</td>                        
                        <td>Voucher Telkomsel Rp. 200.000</td>
                                                           
                        <td>
                            <ul class="nav-icon-action">
                                <li><div class="badge-status"><span class="badge green">Paid</span></div></li>
                                
                            </ul>
                        </td>
                        <td>
                            <ul class="nav-icon-action">
                                <li><a href="<?php echo site_url() ?>/admin_finance/viewDetaildatareimbnonproj" title="view detail" class="btn white-text">Detail</a></li>
                                
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>Ali</td>
                        <td>03/11/2016</td> 
                        <td>Entertainment</td>                       
                        <td>Kemaren beli voucher tapi g ada struknya</td>
                                                         
                        <td>
                            <ul class="nav-icon-action">
                                <li><div class="badge-status"><span class="badge grey">Unpaid</span></div></li>
                                
                            </ul>
                        </td>
                        <td>
                            <ul class="nav-icon-action">
                                <li><a href="<?php echo site_url() ?>/admin_finance/viewDetaildatareimbentertain" title="view detail" class="btn white-text">Detail</a></li>
                                
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td>Dudi</td>
                        <td>07/11/2016</td>
                        <td>Project</td>                        
                        <td>Kemaren pake voucher taxi dari Jakarta Ke Cibitung</td>
                                                        
                        <td>
                            <ul class="nav-icon-action">
                                <li><div class="badge-status"><span class="badge yellow">Pending</span></div></li>
                                
                            </ul>
                        </td>
                        <td>
                            <ul class="nav-icon-action">
                                <li><a href="<?php echo site_url() ?>/admin_finance/viewDetaildatareimbproj" title="view detail" class="btn white-text">Detail</a></li>
                                
                            </ul>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</main>
<div class="clearfix"></div>

           
<?php $this->load->view('finance/footer'); ?>   

        
   