<?php $this->load->view('finance/header'); ?>
<main class="mn-inner" style="padding:0 20px;">
            <div class="row">
              <div class="center-pos">
                <div class="col s12 m4 l3">
                    <div class="card">
                        <div class="card-content center-align">
                            <img src="<?php echo base_url() ?>assets/images/profile-image-2.png" class="responsive-img circle" width="128px" alt="">
                            <p class="m-t-lg flow-text">Desmon William</p>
                            <div class="chip m-t-sm blue-grey white-text">UX/UI Designer</div>
                            
                        </div>
                    </div>

                    
                </div>
              </div>

                <div class="col s12 m12 l9">

                    <div class="card">
                        <div class="card-content ">
                        <div class="right-align">Status : <div class=" chip m-t-sm blue white-text" data-id="badge">New</div> <span id="print" style="display:none"><a href="printout-cuti.html" class="waves-effect waves-grey btn-flat "><i class="material-icons">print</i></a></span></div>
                            <div class="row">
                              <div class="col s3 no-padding">
                                <label>Supervisor</label>
                                <input type="text" name="" disabled value="Intan Gustiarti" class="rounded">
                              </div>
                            </div>

                            <table class="responsive-table bordered table-border">
                              <tr>
                                <td rowspan="2">No</td>
                                <th colspan="2" scope="colgroup">Pemberian Entertainment</th>
                                <th colspan="3" scope="colgroup">Relasi Usaha Entertainment</th>
                                <th rowspan="2" colspan="1" scope="colgroup">Project Description</th>
                                <th rowspan="2" colspan="1" scope="colgroup">Keterangan</th>
                              </tr>
                              <tr>
                                <th scope="col">Tanggal</th>
                                <th scope="col">Jumlah(Rp)</th>
                                <th scope="col">Nama</th>
                                <th scope="col">Posisi</th>
                                <th scope="col">Nama Perusahaan</th>
                                
                              </tr>
                              
                              <tr>
                                <th scope="row">1</th>
                                <td>20-Dec-2015</td>
                                <td>100,000</td>
                                <td>Dimas, Aditya, Hermansyah</td>
                                <td>Self Care, CCM</td>
                                <td>Telkomsel</td>
                                <td>TCM</td>
                                <td>Desmon</td>
                              </tr>
                              <tr>
                                <th scope="row">2</th>
                                <td>25-Dec-2016</td>                                
                                <td>120,000</td>
                                <td>Dimas, Aditya, Hermansyah</td>
                                <td>Self Care, CCM</td>
                                <td>XL</td>
                                <td>GEO</td>
                                 <td>Desmon</td>
                              </tr>
                            </table>
                            
                           <ul class="nav-icon-action center-align">
                                <li id="approval"><a href="javascript:;" data-id="approved" title="approve" class="waves-effect waves-grey btn green white-text ">Terima</a></li>
                                <li id="rejected"><a href="javascript:;" title="tolak" class="waves-effect waves-grey btn-flat ">
                                    Tolak</a></li>
                                
                            </ul>
                        </div>
                        
                        
                    </div>
                </div>



            </div>
</main>

<div class="clearfix"></div>

<?php $this->load->view('finance/footer'); ?>   