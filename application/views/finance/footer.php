            <footer>
                <div class="center-align">Copyright &copy; 2016 | Asia Quattro</div>
            </footer>
        </div>
 
        
        <!-- Javascripts -->
        <script src="<?php echo base_url();?>assets/plugins/jquery/jquery-2.2.0.min.js"></script>        
        <script src="<?php echo base_url();?>assets/plugins/materialize/js/materialize.min.js"></script>
        <script src="<?php echo base_url();?>assets/plugins/material-preloader/js/materialPreloader.min.js"></script>        
        <script src="<?php echo base_url();?>assets/plugins/jquery-blockui/jquery.blockui.js"></script>

        <script src="<?php echo base_url();?>assets/js/bootstrap-editable.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                //toggle `popup` / `inline` mode
                $.fn.editable.defaults.mode = 'inline';     
                //defaults
                // $.fn.editable.defaults.url = '/post'; 
                //make username editable
                $('[data-edit="editable"]').editable();
                $('.myeditable').editable(); 

                // $('.edit-post-button').on('click', function(e) {
               
                //     $('#employee .editable').editable('toggle'); 
                        
                // });

                $(".trigger-status").click(function(){
                    $("#divStatus").toggle();
                });
                
                $(".closeTrigger").click(function(){
                    $("#divStatus").fadeOut();
                });
                
            });
           
        </script>
        <script src="<?php echo base_url();?>assets/js/alpha.min.js"></script>
        <script src="<?php echo base_url();?>assets/plugins/datatables/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $('.datatable-example').DataTable({
                    language: {
                        searchPlaceholder: 'Search records',
                        sSearch: '',
                        sLengthMenu: 'Show _MENU_',
                        sLength: 'dataTables_length',
                        oPaginate: {
                            sFirst: '<i class="material-icons">chevron_left</i>',
                            sPrevious: '<i class="material-icons">chevron_left</i>',
                            sNext: '<i class="material-icons">chevron_right</i>',
                            sLast: '<i class="material-icons">chevron_right</i>' 
                    }
                    }
                });
                $('.dataTables_length select').addClass('browser-default');
            });
            document.getElementById('annual').addEventListener('change', function () {
                var style = this.value == 3 ? 'block' : 'none';
                document.getElementById('special-leave').style.display = style;
              });
        </script>
        <script src="<?php echo base_url();?>assets/js/pages/form_elements.js"></script>
        <script src="<?php echo base_url();?>assets/plugins/sweetalert/sweetalert.min.js"></script>      
        <script src="<?php echo base_url();?>assets/js/pages/miscellaneous-sweetalert.js"></script>
        <script type="text/javascript">

        $('[data-id="approve"]').click(function(){
                swal({   
                    title: "Are you sure?",
                    text: "Please be wise!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#4CAF50",
                    confirmButtonText: "Yes, do it!",
                    cancelButtonText: "No, cancel please!",
                    closeOnConfirm: false,
                    closeOnCancel: false 
                }, function(isConfirm){
                    if (isConfirm) {
                        // swal("Success!", "You have Approved it.", "success");
                        swal({
                          title: "Enter Comments and Decisions!",
                          text: "<textarea id='comments' class='materialize-textarea' style='width:93%' length='120'></textarea>",
                          html: true,
                          showCancelButton: true,
                          closeOnConfirm: false,
                          }, function(inputValue) {
                          if (inputValue === false) return false;
                          if (inputValue === "") {
                            swal.showInputError("You need to write Comments!");
                            return false
                          }
                          // get value using textarea id
                          var val = document.getElementById('comments').value;
                          swal("Success!", "Your Comments: " + val, "success");
                        });
                        $("[data-id='badge']").text('Diterima');
                        $("[data-id='badge']").removeClass('blue');
                        $("[data-id='badge']").addClass('green');
                        $("#rejected").hide();
                        $("#approval").hide();
                        $("#print").show();   
                    } else {
                        swal("Cancelled", "You do not approve it", "error");
                    }
                });
          });

        $('[data-id="approved"]').click(function(){
                swal({   
                    title: "Are you sure?",
                    text: "Please be wise!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#4CAF50",
                    confirmButtonText: "Yes, do it!",
                    cancelButtonText: "No, cancel please!",
                    closeOnConfirm: false,
                    closeOnCancel: false 
                }, function(isConfirm){
                    if (isConfirm) {
                        swal("Success!", "You have Approved it.", "success");                       
                        $("[data-id='badge']").text('Diterima');
                        $("[data-id='badge']").removeClass('blue');
                        $("[data-id='badge']").addClass('green');
                        $("#rejected").hide();
                        $("#approval").hide();
                        $("#print").show();   
                    } else {
                        swal("Cancelled", "You do not approve it", "error");
                    }
                });
          });

            $('[data-id="rejected"]').click(function(){
                    swal({   
                        title: "Are you sure?",
                        text: "Please be wise!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#4CAF50",
                        confirmButtonText: "Yes, do it!",
                        cancelButtonText: "No, cancel please!",
                        closeOnConfirm: false,
                        closeOnCancel: false 
                    }, function(isConfirm){
                        if (isConfirm) {
                            swal("Success!", "You have reject it.", "success");
                            $("[data-id='badge2']").text('Ditolak');
                            $("[data-id='badge2']").removeClass('blue');
                            $("[data-id='badge2']").addClass('red');
                            $("#rejected2").hide();
                            $("#approval2").hide();   
                        } else {
                            swal("Cancelled", "You do not approve it", "error");
                        }
                    });
              });
          

          $(".dropthis").click(function(){
                $(".content-drop").slideToggle();
          });

         
        </script>


        
    </body>
</html>