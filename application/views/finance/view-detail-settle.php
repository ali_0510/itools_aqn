<?php $this->load->view('finance/header'); ?>
<main class="mn-inner">
            <div class="row">

                <div class="col s12 m4 l3">
                    <div class="card">
                        <div class="card-content center-align">
                            <img src="<?php echo base_url() ?>assets/images/profile-image-2.png" class="responsive-img circle" width="128px" alt="">
                            <p class="m-t-lg flow-text">Desmon William</p>
                            <div class="chip m-t-sm blue-grey white-text">UX/UI Designer</div>
                            
                        </div>
                    </div>

                
                </div>
                <div class="col s12 m4 l9">
                    <div class="card">
                        <div class="card-content ">
                        <div class="right-align">Status : <div class=" chip m-t-sm blue white-text" data-id="badge">New</div></div>
                            <div class="row">
                              <div class="col s3 no-padding">
                                <label>Supervisor</label>
                                <input type="text" name="" disabled value="Intan Gustiarti" class="rounded">
                              </div>
                            </div>
                            <div class="row">
                              <div class="col s3 no-padding" style="margin-right:5%">
                                <label>Cash Advance</label>
                                <input type="text" name="" disabled value="Rp 10.000.000" class="rounded">
                              </div>

                              <div class="col s3 no-padding">
                                <label>Settlement</label>
                                <input type="text" name="" disabled value="Rp 200.000" class="rounded">
                              </div>
                            </div>
                            
                            <!-- <table id="example" class="display responsive-table datatable-example bordered">
                              <thead>
                                  <tr>
                                      <th>No</th>
                                      <th>Date Submit</th>
                                      <th>Date CA</th>
                                      <th>Projects</th>
                                      <th>Ammount CA</th>
                                      <th width="250">Description</th>
                                      
                                  </tr>
                              </thead>
                              <tfoot>
                                  <tr>
                                      <th>No</th>
                                      <th>Date Submit</th>
                                      <th>Date CA</th>
                                      <th>Projects</th>
                                      <th>Ammount CA</th>
                                      <th width="250">Description</th>
                                      
                                  </tr>
                              </tfoot>
                              <tbody>
                                  <tr>
                                      <td>1</td>
                                      <td>11/11/2016</td> 
                                      <td>15/11/2016</td>
                                      <td>TCM</td>
                                      <td>Rp 2.000.000</td>                         
                                      <td>Voucher Xl 100</td>
                                  </tr>
                                  <tr>
                                      <td>2</td>
                                      <td>08/11/2016</td>                        
                                      <td>15/11/2016</td>
                                      <td>Others</td>
                                      <td>Rp 2.000.000</td>                         
                                      <td>Voucher Xl 100</td>
                                  </tr>
                                  <tr>
                                      <td>3</td>
                                      <td>03/11/2016</td>                        
                                      <td>15/11/2016</td>
                                      <td>Telkomsel</td>
                                      <td>Rp 2.000.000</td>                         
                                      <td>Voucher Xl 100 Indosat 100</td>
                                  </tr>
                                  <tr>
                                      <td>4</td>
                                      <td>07/11/2016</td>                        
                                      <td>15/11/2016</td>
                                      <td>Others</td>
                                      <td>Rp 3.800.000</td>                         
                                      <td>Voucher Indosat 50</td>
                                  </tr>
                              </tbody>
                          </table> -->
                        </div>
                        
                        
                    </div>
                </div>

            </div>
</main>

<div class="clearfix"></div>

<?php $this->load->view('finance/footer'); ?>   