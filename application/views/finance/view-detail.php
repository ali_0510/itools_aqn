<?php $this->load->view('finance/header'); ?>
            <main class="mn-inner">
                <div class="row">
                    <div class="col s12">
                       
                        <div class="row">

                            <div class="col s12 m4 l3">
                                <div class="card">
                                    <div class="card-content center-align">
                                        <img src="<?php echo base_url() ?>assets/images/profile-image-2.png" class="responsive-img circle" width="128px" alt="">
                                        <p class="m-t-lg flow-text">John Doe</p>
                                        <div class="chip m-t-sm blue-grey white-text">UI Designer</div> 
                                        
                                    </div>
                                </div>
                            
                            </div>
                            <div class="col s12 m4 l9">
                                
                                        <ul class="collection" style="border:none">
                                          <li class="collection-item"><span class="cl_name">Nama</span><span class="cl_value">John Doe</span></li>
                                          <li class="collection-item"><span class="cl_name">NIK</span><span class="cl_value">PG00001</span></li>
                                          <li class="collection-item"><span class="cl_name">No. KTP</span><span class="cl_value">6928002939293828</span></li>
                                          <li class="collection-item"><span class="cl_name">Telepon</span><span class="cl_value">087766554221</span></li>
                                          <li class="collection-item"><span class="cl_name">Jenis Kelamin</span><span class="cl_value">Laki-Laki</span></li>
                                          <li class="collection-item"><span class="cl_name">Agama</span><span class="cl_value">Islam</span></li>
                                          <li class="collection-item"><span class="cl_name">Status</span><span class="cl_value">Menikah</span></li>
                                          <li class="collection-item"><span class="cl_name">Alamat</span><span class="cl_value">Jl. Raya Citayam RT 002/002 No. 49B  Pondok Terong Cipayung Depok JawaBarat </span></li>
                                      </ul>
                                    
                            </div>
                            
                        </div>
                    </div>

                    <div class="row">
                        <div class="col s12">
                            <ul class="tabs tab-demo z-depth-1" style="width: 100%;">
                                <li class="tab col s3"><a href="#test1" class="">Annual History</a></li>
                                <li class="tab col s3"><a class="" href="#test2">Reimburse History</a></li>
                                <li class="tab col s3"><a class="" href="#test3">CA History</a></li>
                            </ul>
                        </div>
                        <div id="test1" class="col s12">

                            
                            <table class="display responsive-table datatable-example bordered">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>Particulars</th>
                                        <th>Tgl Pengajuan</th>
                                        <th>Periode Cuti</th>
                                        <th>Jumlah Cuti</th>
                                        <th>Pengganti Cuti</th>
                                        <th>Keterangan Cuti</th>
                                        <th>Status</th>
                                        
                                    </tr>
                                </thead>
                                <tfoot>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama</th>
                                            <th>Particulars</th>
                                            <th>Tgl Pengajuan</th>
                                            <th>Periode Cuti</th>
                                            <th>Jumlah Cuti</th>
                                            <th>Pengganti Cuti</th>
                                            <th>Keterangan Cuti</th>
                                            <th>Status</th>
                                            
                                        </tr>
                                </tfoot>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>Andi</td>
                                        <td>Annual Leave</td>
                                        <td>11/11/2016</td>
                                        <td>12/11/2016 - 20/11/2016</td>
                                        <td>3 Hari</td>
                                        <td>Megawati</td>
                                        <td>Lorem Ipsum Dolor Siat Amet</td>
                                        <td><div class="badge-status"><span class="badge green">Diterima</span></div></td>
                                        
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>Imam</td>
                                        <td>Annual Leave</td>
                                        <td>11/12/2016</td>
                                        <td>12/12/2016 - 20/12/2016</td>
                                        <td>8 Hari</td>
                                        <td>Ivan</td>
                                        <td>Lorem Ipsum Dolor Siat Amet</td>
                                        <td><div class="badge-status"><span class="badge red">Ditolak</span></div></td>
                                       
                                    </tr>
                                </tbody>
                            </table>
                   

                    </div>

                    <div id="test2" class="col s12">
                       
                                
                                <table class="display responsive-table datatable-example bordered">
                                    <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Name</th>
                                                <th>Date Submit</th>
                                                <th>Date of Bill</th>
                                                <th>Type</th>
                                                <th width="250">Description</th>
                                                <th>Amount</th>
                                                <th>Status</th>
                                                
                                            </tr>
                                    </thead>
                                    <tfoot>
                                            <tr>
                                                <th>No</th>
                                                <th>Name</th>
                                                <th>Date Submit</th>
                                                <th>Date of Bill</th>
                                                <th>Type</th>
                                                <th width="250">Description</th>
                                                <th>Amount</th>
                                                <th>Status</th>
                                                
                                            </tr>
                                    </tfoot>
                                    <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>Desmon William</td>
                                                <td>11/11/2016</td>
                                                <td>14/11/2016</td>
                                                <td>Voucher Telkomsel</td>
                                                <td>Lorem ipsum dolor sit amet, consectetur adipisicing elit. </td>
                                                <td>$320,800</td>
                                                <td><div class="badge-status"><span class="badge green">Diterima</span></div></td>
                                                
                                            </tr>
                                            <tr>
                                                <td>2</td>
                                                <td>Desmon William</td>
                                                <td>15/11/2016</td>
                                                <td>19/11/2016</td>
                                                <td>Taxi</td>
                                                <td>Ut enim ad minim veniam, quis nostrud exercitation ullamco.</td>
                                                <td>$170,750</td>
                                                <td><div class="badge-status"><span class="badge green">Diterima</span></div></td>
                                                
                                            </tr>
                                    </tbody>
                                </table>
                    
                    </div>


                    <div id="test3" class="col s12">
                            <table  class="display datatable-example responsive-table highlight striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Name</th>
                                        <th>Date Submit</th>            
                                        <th>Date CA</th>
                                        <th>Amount CA</th>
                                        <th>Projects</th>
                                        <th width="250">Description</th>            
                                        <th>Status</th>
                                        
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>No</th>
                                        <th>Name</th>
                                        <th>Date Submit</th>            
                                        <th>Date CA</th>
                                        <th>Amount CA</th>
                                        <th>Projects</th>
                                        <th width="250">Description</th>            
                                        <th>Status</th>
                                        
                                    </tr>
                                </tfoot>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>Desmon William</td>
                                        <td>11/11/2016</td>
                                        <td>15/11/2016</td>
                                        <td>$320,800</td>
                                        <td>Kineria</td>
                                        <td>1 unit Lenovo E450</td>            
                                        <td><div class="badge-status"><span class="badge green">Diterima</span></div></td>
                                        
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>Desmon William</td>
                                        <td>16/11/2016</td>
                                        <td>18/11/2016</td>
                                        <td>$320,800</td>
                                        <td>Telkomsel</td>            
                                        <td>1 unit Lenovo E450</td>            
                                        <td><div class="badge-status"><span class="badge green">Diterima</span></div></td>
                                        
                                    </tr>
                                   
                                </tbody>
                            </table>
                        </div>
                        
                </div>


                    


                    

                </div>
            </main>
            <div class="clearfix"></div>
<?php $this->load->view('finance/footer'); ?>   


<!-- <main class="mn-inner">
                <div class="row">

                    <div class="col s12 m4 l3">
                        <div class="card">
                            <div class="card-content center-align">
                                <img src="<?php echo base_url() ?>assets/images/profile-image-2.png" class="responsive-img circle" width="128px" alt="">
                                <p class="m-t-lg flow-text">David Wade</p>
                                <div class="chip m-t-sm blue-grey white-text">UI Designer</div> 
                                <div class="chip m-t-sm blue-grey white-text">Developer</div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-content">
                                <span class="card-title">Circles</span>
                                <div class="chip m-t-sm">Envato Market</div> 
                                <div class="chip m-t-sm">Steelcoders</div>
                                <div class="chip m-t-sm">Family</div>
                                <div class="chip m-t-sm">Work</div>
                                <div class="chip m-t-sm">Friends</div>
                            </div>
                        </div>
                    </div>
                    <div class="col s12 m4 l5">
                        <div class="card">
                            <div class="card-content ">
                                
                            </div>
                            <div class="card-content ">
                                <span class="card-title">Contact Info</span>
                                <p>Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col s12 m4 l4">
                        <div class="card">
                            <div class="card-content ">
                                <span class="card-title">Address</span>
                                <address>
                                    <strong>Twitter, Inc.</strong><br>
                                    795 Folsom Ave, Suite 600<br>
                                    San Francisco, CA 94107<br>
                                    <abbr title="Phone">P:</abbr> (123) 456-7890
                                </address>
                            </div>
                            <div class="card-content ">
                                
                            </div>
                        </div>
                    </div>
                </div>
    </main> -->