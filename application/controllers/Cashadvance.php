<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Cashadvance extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library(array('ion_auth'));		
        $this->load->model('cashadvance_model','cashadvance');

	}

	// redirect if needed, otherwise display the user list
	public function index()
	{

		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		else
		{	
			$user = $this->ion_auth->user()->row();

			//$this->ion_auth->register("iim", "iim123", "iim@asiaquatro.net", array(), array(2));

			$this->load->view('tpl/cashadvance/form-pengajuan-ca', $user);
		}
	}

	public function admin()
	{
		if (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{	
			$user = $this->ion_auth->user()->row();

			//$this->ion_auth->register("iim", "iim123", "iim@asiaquatro.net", array(), array(2));

			$this->load->view('admin/ca-data', $user);
		}
	}


}
