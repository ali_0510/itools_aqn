<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_Finance extends CI_Controller {


	
	public function annualdata()
	{

		$this->load->view('finance/annual-data');
	}
	
	public function reimbursement()
	{

		$this->load->view('finance/reimbursement');
	}
	public function reimbursedata()
	{

		$this->load->view('finance/reimburse-data');
	}
	
	public function cashAdvanceData()
	{

		$this->load->view('finance/ca-data');
	}
	public function settlement()
	{
		$this->load->view('finance/settlement');
	}
	public function settlementData()
	{

		$this->load->view('finance/settlement-data');
	}
	public function employeeData()
	{

		$this->load->view('finance/employee-data');
	}
	public function viewDetail()
	{

		$this->load->view('finance/view-detail');
	}
	public function viewDetaildata()
	{

		$this->load->view('finance/view-detail-data');
	}
	public function viewDetaildatareimb()
	{

		$this->load->view('finance/view-detail-reimb');
	}
	public function viewDetaildatareimbproj()
	{

		$this->load->view('finance/view-detail-reimbproj');
	}
	public function viewDetaildatareimbnonproj()
	{

		$this->load->view('finance/view-detail-reimbnonproj');
	}
	public function viewDetaildatareimbentertain()
	{

		$this->load->view('finance/view-detail-reimbentertain');
	}
	public function viewDetaildataca()
	{

		$this->load->view('finance/view-detail-ca');
	}
	public function viewDetaildatasettle()
	{

		$this->load->view('finance/view-detail-settle');
	}
	public function addUser()
	{

		$this->load->view('finance/add-user');
	}
	public function adminAccount()
	{
		$this->load->view('finance/myaccount');
	}
	
}
