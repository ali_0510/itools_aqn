<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {


	
	public function annualdata()
	{

		$this->load->view('admin/annual-data');
	}
	public function annual()
	{

		$this->load->view('admin/annual');
	}
	public function reimbursement()
	{

		$this->load->view('admin/reimbursement');
	}
	public function reimbursedata()
	{

		$this->load->view('admin/reimburse-data');
	}
	public function cashAdvance()
	{
		$this->load->view('admin/cash-advance');
	}
	
	public function cashAdvanceData()
	{

		$this->load->view('admin/ca-data');
	}
	public function settlement()
	{
		$this->load->view('admin/settlement');
	}
	public function settlementData()
	{

		$this->load->view('admin/settlement-data');
	}
	public function employeeData()
	{

		$this->load->view('admin/employee-data');
	}
	public function viewDetail()
	{

		$this->load->view('admin/view-detail');
	}
	public function viewDetaildata()
	{

		$this->load->view('admin/view-detail-data');
	}
	public function viewDetaildatareimb()
	{

		$this->load->view('admin/view-detail-reimb');
	}
	public function viewDetaildatareimbproj()
	{

		$this->load->view('admin/view-detail-reimbproj');
	}
	public function viewDetaildatareimbnonproj()
	{

		$this->load->view('admin/view-detail-reimbnonproj');
	}
	public function viewDetaildatareimbentertain()
	{

		$this->load->view('admin/view-detail-reimbentertain');
	}
	public function viewDetaildataca()
	{

		$this->load->view('admin/view-detail-ca');
	}
	public function viewDetaildatasettle()
	{

		$this->load->view('admin/view-detail-settle');
	}
	public function addUser()
	{

		$this->load->view('admin/add-user');
	}
	public function adminAccount()
	{
		$this->load->view('admin/myaccount');
	}
	
}
