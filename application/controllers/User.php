<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function annual()
	{

		$this->load->view('user/annual');
	}
	public function reimbursement()
	{

		$this->load->view('user/reimbursement');
	}

	public function reimbursementproj()
	{

		$this->load->view('user/reimbursement-proj');
	}
	public function reimbursementnonproj()
	{

		$this->load->view('user/reimbursement-nonproj');
	}
	public function reimbursemententertain()
	{

		$this->load->view('user/reimbursement-entertainment');
	}

	public function ajxAnnual()
	{

		$this->load->view('user/ajax/annual-history');
	}
	public function ajxReimb()
	{

		$this->load->view('user/ajax/reimburse-history');
	}
	public function ajxCa()
	{

		$this->load->view('user/ajax/ca-history');
	}
	public function ajxSt()
	{

		$this->load->view('user/ajax/settlement-history');
	}
	public function history()
	{

		$this->load->view('user/history');
	}

	public function printoutCuti()
	{
		$this->load->view('user/printout-cuti');
		
	}
	public function printoutReimb()
	{
		$this->load->view('user/printout-reimburse');
	}
	public function printoutReimbproj()
	{
		$this->load->view('user/printout-reimburse-project');
	}
	public function printoutReimbent()
	{
		$this->load->view('user/printout-reimburse-entertain');
	}
	public function printoutSettlement()
	{
		$this->load->view('user/printout-settlement');
	}
	public function cashAdvance()
	{
		$this->load->view('user/cash-advance');
	}
	public function settlement()
	{
		$this->load->view('user/settlement');
	}
	public function printoutCa()
	{
		$this->load->view('user/printout-ca');
	}
	
	public function myAccount()
	{
		$this->load->view('user/my-account');
	}

}
