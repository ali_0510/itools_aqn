<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library(array('ion_auth'));

	}

	// redirect if needed, otherwise display the user list
	public function index()
	{

		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		else
		{

			$user = $this->ion_auth->user()->row();
			$this->load->view('tpl/home', $user);
		}	

		//$user_groups = $this->ion_auth->get_users_groups($user->id)->result();
		//var_dump($user_groups);
		//$this->ion_auth->register("iim", "iim123", "iim@asiaquatro.net", array(), array(2));

	}


}
