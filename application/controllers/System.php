<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class System extends CI_Controller {

	
	public function index()
	{
		$this->load->view('login');
	}

	public function checklogin(){
		$datalogin = array(
			'username'=>$this->input->post('uname'),
			'password'=>$this->input->post('pass')
			);
		$result = $this->Model->checklogin($datalogin);
		if($result){
			if($result->lvl == '1'){
				$this->adminpanel();
			}else{
				$this->userpanel();
			}
		}else{
			echo "<script>alert('Not found')</script>";
		}
	}

	public function userpanel(){
		$this->load->view('user/user');
	}

	public function adminpanel(){
		$this->load->view('admin/admin');
	}

	
}
