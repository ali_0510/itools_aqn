<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_Model
{
	private $table_users = 'users';
	private $table_groups = 'groups';
	private $table_users_groups = 'users_groups';

	public function __construct()
	{
		parent::__construct();
	}

	public function supervisorList()
	{
		$this->db->select('a.id, a.nama_lengkap');
        $this->db->from($this->table_users." a");
		$this->db->join($this->table_users_groups." b", "a.id = b.user_id");
        $this->db->where("b.group_id", 2);          
        $this->db->order_by("a.nama_lengkap ASC");
        $query = $this->db->get();
        return $query->result();
	}

	
}