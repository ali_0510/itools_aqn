<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cuti_model extends CI_Model
{
	private $table_pengajuan_cuti= 'pengajuan_cuti';
	private $table_saldo_cuti = 'saldo_cuti';
	private $table_jenis_cuti = 'jenis_cuti';

	public function __construct()
	{
		parent::__construct();
	}

	
	public function jenisCuti()
	{
		$this->db->select('id, nama');
        $this->db->from($this->table_jenis_cuti);
        $this->db->order_by("id ASC");
        $query = $this->db->get();
        return $query->result();
	}

}