<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class General_model extends CI_Model
{
	private $table_code_seq = 'code_seq';

	public function __construct()
	{
		parent::__construct();
	}

	public function getCodeSeq($name)
	{

		$this->db->select('name, code_seq');
        $this->db->from($this->table_code_seq);
        $this->db->where("name", $name);          
        $query = $this->db->get();
        $row = $query->row();
        if (!empty($row))
        {
        	$name = $row->name;
	        $seq = $row->code_seq + 1;
	        
	        $code = "{$name}".sprintf("%04d", $seq);
	        
	        $this->db->where("name", $name);  
        	$this->db->update($this->table_code_seq, array('code_seq' => $seq));
	        
	        return $code;
        }

        return false;
	}

	
}