INSERT INTO `groups` (`id`, `name`, `description`) VALUES
     (1,'admin','Administrator'),
     (2,'pimpinan','Pimipinan'),
     (3,'karyawan','Karyawan'),
     (4,'pm','Project Manager');

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `created_on`, `last_login`, `active`, `nama_lengkap`, `jabatan`, `tanggal_masuk`, `nik`, `no_ktp`, `telepon`, `jenis_kelamin`, `agama`, `status`, `alamat`) VALUES
     ('1','127.0.0.1','administrator','$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36','','admin@admin.com','',NULL,'1268889823','1268889823','1', 'Administrator', null, null, null, null, null, null, null, null, null),
	 ('2','127.0.0.1','iim','$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36','','iim@asiaquatro.net','',NULL,'1268889823','1268889823','1', 'Iim Irvan Ibrahim', 'Senior Developer', '2010/10/01', null, null, null, 'Laki-laki', 'Islam', 'Menikah', 'Bekasi'),
	 ('3','127.0.0.1','dudi','$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36','','dudi.w@asiaquatro.net','',NULL,'1268889823','1268889823','1', 'Dudi W', 'Junior Developer', '2016/08/01', null, null, null, 'Laki-laki', 'Islam', 'Menikah', 'Jakarta'),
	 ('4','127.0.0.1','adit','$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36','','aditya.pratama@asiaquatro.net','',NULL,'1268889823','1268889823','1', 'Aditya Pratama', 'Junior Developer', '2016/08/01', null, null, null, 'Laki-laki', 'Islam', 'Menikah', 'Jakarta'),
	 ('5','127.0.0.1','ali','$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36','','ali.s@asiaquatro.net','',NULL,'1268889823','1268889823','1', 'Ahmad Suhaili', 'Frontend Developer', '2016/07/01', null, null, null, 'Laki-laki', 'Islam', 'Menikah', 'Jakarta'),
	 ('6','127.0.0.1','saut','$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36','','sautms@asiaquatro.net','',NULL,'1268889823','1268889823','1', 'Saut M. Simanjuntak', 'Head of Technical Support and Operations', '2010/01/01', null, null, null, 'Laki-laki', '', 'Menikah', 'Jakarta');

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
     (1,1,1),
     (2,2,3),
     (3,3,3),
     (4,4,3),
     (5,5,3),
     (6,6,2);

INSERT INTO `jenis_cuti` (`id`, `nama`) VALUES
(1, 'Annual Leave'),
(2, 'Unpaid Leave'),
(3, 'Special Leave');

INSERT INTO `saldo_cuti` (`id`, `user_id`, `jenis_cuti`, `terpakai`, `saldo`, `expired`) VALUES
(1, 2, 1, 0, 12, '2016/12/31'),
(2, 3, 1, 0, 12, '2016/12/31'),
(3, 4, 1, 0, 12, '2016/12/31'),
(4, 5, 1, 0, 12, '2016/12/31'),
(5, 6, 1, 0, 12, '2016/12/31');


INSERT INTO `project_code` (`id`, `name`, `code`) VALUES
(1, 'Telkomsel TCM', 'TCM0001'),
(2, 'XL Geolocation', 'GEO0002'),
(3, 'HUTCH POSTR', 'PST0003');


INSERT INTO `code_seq` (`id`, `name`, `code_seq`) VALUES
(1, 'CA', '0001');